//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKTableViewCellDefault.h"

#import "KMKCellLayoutParams.h"


// Description of layout.
typedef struct {
    CGRect textFrame;
    CGRect imageFrame;
    CGFloat preferredHeight;
} KMKTableViewCellDefaultLayoutInfo;


@interface KMKTableViewCellDefault (Subclass)

/**
 Used to make layout.
 */
+ (KMKTableViewCellDefaultLayoutInfo)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                                cellSize:(CGSize)size;

@end
