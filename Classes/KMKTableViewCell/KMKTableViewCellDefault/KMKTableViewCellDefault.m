//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKTableViewCellDefault.h"

#import "NSString+KMKUtils.h"

#import "KMKTableViewCellDefault+Subclass.h"
#import "KMKTblCellDefaultLayoutParamsImpl.h"
#import "KMKCellConstants.h"


@implementation KMKTableViewCellDefault

#pragma mark - Class methods

+ (CGFloat)preferredHeightForText:(NSString *)text
                             font:(UIFont *)font
                    numberOfLines:(NSInteger)numberOfLines
                            image:(UIImage *)image
                            width:(CGFloat)width {
    KMKTblCellDefaultLayoutParamsImpl *layoutParams = [KMKTblCellDefaultLayoutParamsImpl new];
    layoutParams.text = text;
    layoutParams.textNumberOfLines = numberOfLines;
    layoutParams.textFont = font;
    layoutParams.image = image;
    CGFloat height = [self preferredHeightForDataSource:layoutParams width:width];
    return height;
}

#pragma mark - Allocation

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithCellStyle:KMKTableViewCellStyleDefault reuseIdentifier:nil layoutMaker:nil];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [self initWithReuseIdentifier:nil];
    if (self) {
        self.frame = frame;
    }
    return self;
}

- (id)init {
    self = [self initWithReuseIdentifier:nil];
    if (self) {
    }
    return self;
}

#pragma mark -
#pragma mark Protected

- (CGSize)sizeThatFits:(CGSize)size {
    KMKTableViewCellDefaultLayoutInfo layoutInfo = [[self class] layoutInfoForParams:[self layoutParams]
                                                                                       cellSize:size];
    CGSize resultSize = CGSizeMake(size.width, layoutInfo.preferredHeight);
    return resultSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    KMKTableViewCellDefaultLayoutInfo layoutInfo = [[self class] layoutInfoForParams:[self layoutParams]
                                                                            cellSize:self.bounds.size];
    self.imageView.frame = layoutInfo.imageFrame;
    self.textLabel.frame = layoutInfo.textFrame;
}

#pragma mark
#pragma mark Private

+ (KMKTableViewCellDefaultLayoutInfo)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                                cellSize:(CGSize)size {
    KMKTableViewCellDefaultLayoutInfo layoutInfo;
    
    const CGFloat kImageTopMargin = KMKCellTopMargin;
    const CGFloat kImageBottomMargin = KMKCellBottomMargin;
    
    CGRect imageFrame = CGRectZero;
    imageFrame.size = [params image].size;
    imageFrame.origin.y = floorf((size.height - imageFrame.size.height)/2.0);
    imageFrame.origin.x = KMKCellLeftMargin;
    layoutInfo.imageFrame = imageFrame;
    
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = imageFrame.origin.x + imageFrame.size.width + (params.image ? KMKCellHorizontalSpacing : 0.0);
    textFrame.size.width = size.width - textFrame.origin.x - KMKCellRightMargin;
    
    const NSInteger kNumberOfLines = ([params textNumberOfLines] == 0 ? NSIntegerMax : [params textNumberOfLines]);
    CGSize constrainingSize = CGSizeMake(textFrame.size.width, kNumberOfLines * [[params textFont] lineHeight]);
    
    CGSize textSize = [params.text kmk_multilineSizeConstrainedToSize:constrainingSize
                                                                 font:params.textFont
                                                        lineBreakMode:params.textLineBreakMode];
    
    textFrame.origin.y = floorf((size.height - textSize.height)/2.0);
    textFrame.size.height = textSize.height;
    layoutInfo.textFrame = textFrame;
    
    layoutInfo.preferredHeight = MAX(kImageTopMargin + imageFrame.size.height + kImageBottomMargin,
                                     KMKCellTopMargin + textSize.height + KMKCellBottomMargin);
    return layoutInfo;
}

- (KMKTblCellDefaultLayoutParamsImpl *)layoutParams {
    KMKTblCellDefaultLayoutParamsImpl *layoutParams = [KMKTblCellDefaultLayoutParamsImpl new];
    layoutParams.text = self.textLabel.text;
    layoutParams.textNumberOfLines = self.textLabel.numberOfLines;
    layoutParams.textFont = self.textLabel.font;
    layoutParams.image = self.imageView.image;
    return layoutParams;
}

@end
