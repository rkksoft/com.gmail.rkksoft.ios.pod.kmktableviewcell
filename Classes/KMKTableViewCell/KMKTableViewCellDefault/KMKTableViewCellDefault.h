//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"


/**
 Resembles UITableViewCell with UITableViewCellStyleDefault style. But imageView (if it is set) always has the same left margin (KMKCellLeftMargin).
 Allows to calculate the height needed to normally display content. When calculating the height the imageView is taken into account.
 
 The property detailTextLabel is not available.
 */
// TODO: replace all uses of thie class to KMKCell with cellStyle KMKCellStyleDefault
@interface KMKTableViewCellDefault : KMKCell

+ (CGFloat)preferredHeightForText:(NSString *)text
                             font:(UIFont *)font
                    numberOfLines:(NSInteger)numberOfLines
                            image:(UIImage *)image
                            width:(CGFloat)width;

/**
 Designated initilizer.
 */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)initWithFrame:(CGRect)frame;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)init;

/**
 @discussion
     Width is left the same.
 */
- (CGSize)sizeThatFits:(CGSize)size;

@end


