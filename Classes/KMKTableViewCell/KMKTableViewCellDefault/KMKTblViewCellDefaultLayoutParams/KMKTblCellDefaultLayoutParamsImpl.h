//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutParams.h"


@interface KMKTblCellDefaultLayoutParamsImpl : NSObject <KMKCellLayoutParams>

/**
 Cell style.
 */
@property (nonatomic, assign) KMKTableViewCellStyle cellStyle;

/**
 Text of textLabel.
 */
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSAttributedString *attributedText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger textNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *textFont;

/**
 Line break mode of the textLabel.
 */
@property (nonatomic, assign) NSLineBreakMode textLineBreakMode;

#pragma mark `detailTextLabel`

/**
 Text of detailTextLabel.
 */
@property (retain, nonatomic) NSString *detailText;
@property (strong, nonatomic) NSAttributedString *detailAttributedText;

/**
 Max number of lines in detailTextLabel.
 */
@property (nonatomic, assign) NSInteger detailTextNumberOfLines;

/**
 Font of detailTextLabel.
 */
@property (retain, nonatomic) UIFont *detailTextFont;

/**
 lineBreakMode of detailTextLabel.
 */
@property (nonatomic, assign) NSLineBreakMode detailTextLineBreakMode;


/**
 May be nil.
 */
@property (strong, nonatomic) UIImage *image;

/**
 Accessory type of cell.
 */
@property (nonatomic, assign) UITableViewCellAccessoryType accessoryType;

/**
 Accessory view of cell.
 */
@property (strong, nonatomic) UIView *accessoryView;

@end
