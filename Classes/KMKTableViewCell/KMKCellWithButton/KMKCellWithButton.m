//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellWithButton.h"

#import "KMKCellConstants.h"
#import "KMKCellWithButtonLayoutParams.h"
#import "KMKCellWithButtonLayoutInfo.h"
#import "KMKCellWithButtonLayoutMaker.h"


@interface KMKCellWithButton ()

@property (strong, nonatomic) UIButton *button;

@end


@implementation KMKCellWithButton

#pragma mark
#pragma mark Allocation

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier button:(UIButton *)button {
    if (self = [super initWithCellStyle:KMKTableViewCellStyleDefault reuseIdentifier:reuseIdentifier layoutMaker:nil]) {
        _button = button;
        self.contentVerticalAlignment = KMKCellWithButtonContentVerticalAlignmentCenter;
        [self addSubview:_button];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

#pragma mark
#pragma mark Protected

// overriden
- (id<KMKCellLayoutParams>)layoutParams {
    KMKCellWithButtonLayoutParams *params = [KMKCellWithButtonLayoutParams layoutParams];
    params.buttonHeight = self.button.frame.size.height;    
    return params;
}

#pragma mark
#pragma mark Protected

// overriden
- (void)applyLayoutInfo:(id<KMKCellLayoutInfo>)layoutInfo {
    KMKCellWithButtonLayoutInfo *cellWithButtonLayoutInfo = (KMKCellWithButtonLayoutInfo *)layoutInfo;
    self.button.frame = cellWithButtonLayoutInfo.buttonFrame;
}

// overriden
+ (id<KMKCellLayoutMaker>)defaultLayoutMakerForCellStyle:(KMKTableViewCellStyle)cellStyle {
    static KMKCellWithButtonLayoutMaker *layoutMaker = nil;
    if (layoutMaker == nil) {
        layoutMaker = [KMKCellWithButtonLayoutMaker layoutMaker];
    }
    return layoutMaker;
}

@end
