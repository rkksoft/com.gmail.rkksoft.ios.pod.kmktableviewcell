//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutInfoImpl.h"


@interface KMKCellWithButtonLayoutInfo : KMKCellLayoutInfoImpl

@property (nonatomic, assign) CGRect buttonFrame;

@end
