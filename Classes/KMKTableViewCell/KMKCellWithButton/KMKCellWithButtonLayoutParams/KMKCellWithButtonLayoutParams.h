//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutParamsImpl.h"
#import "KMKCellWithButton.h"


/// DTO, containing all parameter needed for calculating layout of cell with button. 
@interface KMKCellWithButtonLayoutParams : KMKCellLayoutParamsImpl

// Button.
@property (nonatomic, assign) CGFloat buttonHeight;

/**
 Vertical alignment of button in cell.
 */
@property (nonatomic, assign) KMKCellWithButtonContentVerticalAlignment contentVerticalAlignment;

@end



