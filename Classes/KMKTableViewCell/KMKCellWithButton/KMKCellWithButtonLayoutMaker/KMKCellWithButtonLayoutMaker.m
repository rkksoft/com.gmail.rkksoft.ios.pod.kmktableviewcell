//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellWithButtonLayoutMaker.h"

#import "KMKCellWithButtonLayoutInfo.h"
#import "KMKCellWithButtonLayoutParams.h"
#import "KMKCellConstants.h"


@implementation KMKCellWithButtonLayoutMaker

- (id<KMKCellLayoutInfo>)layoutInfoForParams: (id<KMKCellLayoutParams>)params
                                    cellSize: (CGSize)size {
    
    KMKCellWithButtonLayoutInfo *layoutInfo = [[KMKCellWithButtonLayoutInfo alloc] init];
    KMKCellWithButtonLayoutParams *cellWithButtonLayoutParams = (KMKCellWithButtonLayoutParams *)params;
    
    CGRect buttonFrame = CGRectZero;
    buttonFrame.origin.x = KMKCellLeftMargin;
    buttonFrame.size.width = size.width - (KMKCellLeftMargin + KMKCellRightMargin);
    buttonFrame.size.height = (cellWithButtonLayoutParams.buttonHeight == 0 ? 30.0 : cellWithButtonLayoutParams.buttonHeight);
    
    CGFloat cellContingentRightHeight = floorf((5/3.0) * buttonFrame.size.height);
    layoutInfo.contingentRightSize = CGSizeMake(size.width, cellContingentRightHeight);
    
    switch (cellWithButtonLayoutParams.contentVerticalAlignment) {
        case KMKCellWithButtonContentVerticalAlignmentTop: {
            buttonFrame.origin.y = 0.0;
            break;
        }
        case KMKCellWithButtonContentVerticalAlignmentCenter: {
            buttonFrame.origin.y = floorf((size.height - buttonFrame.size.height)/2.0);
            break;
        }
        case KMKCellWithButtonContentVerticalAlignmentBottom: {
            buttonFrame.origin.y = size.height - buttonFrame.size.height;
            break;
        }
        default:
            NSAssert(NO, @"The App error!");
            break;
    }
    
    layoutInfo.buttonFrame = buttonFrame;
    
    return layoutInfo;
}

@end
