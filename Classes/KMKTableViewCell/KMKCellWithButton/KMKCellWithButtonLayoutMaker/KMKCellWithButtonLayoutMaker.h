//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutMakerImpl.h"

/**
 The width of the cell is not changed when calculating the contingentRightSize.
 Button's height is not changed.
 */
@interface KMKCellWithButtonLayoutMaker :KMKCellLayoutMakerImpl

@end
