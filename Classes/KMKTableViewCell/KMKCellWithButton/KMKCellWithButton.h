//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

typedef enum{
    KMKCellWithButtonContentVerticalAlignmentCenter  = 0,
    KMKCellWithButtonContentVerticalAlignmentTop     = 1,
    KMKCellWithButtonContentVerticalAlignmentBottom  = 2
//    KMKCellWithButtonContentVerticalAlignmentFill    = 3
} KMKCellWithButtonContentVerticalAlignment;

/**
 Cell which contains only one UI-element - button.
 Frame of button is set when making layout. The height of the button is not changed.
 The height of the button is not changed. If height is 0 then it is set to 30 pt.
 The button spans the entire width of the cell.
 */
@interface KMKCellWithButton : KMKCell

/**
 Designated initializer.
 @param button Button which is displayed in the center of cell.
 */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier button:(UIButton *)button;

/**
 Button passed in initializer.
 */
@property (strong, nonatomic, readonly) UIButton *button;

/**
 Specify vertical alignment of button in cell.
 Default value - KMKCellWithButtonContentVerticalAlignmentCenter.
 */
@property (nonatomic, assign) KMKCellWithButtonContentVerticalAlignment contentVerticalAlignment;

@end
