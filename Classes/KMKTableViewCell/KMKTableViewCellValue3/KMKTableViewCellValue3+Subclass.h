//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellValue3.h"

#import "KMKCell+Subclass.h"


// Description of layout.
typedef struct {
    CGRect textFrame;
    CGRect detailTextFrame;
    CGRect imageFrame;
    CGFloat preferredHeight;
} KMKTableViewCellValue3LayoutInfo;


@interface KMKCellValue3 (Subclass)

+ (KMKTableViewCellValue3LayoutInfo)tableViewCellValue3LayoutInfoForParams:(KMKTableViewCellValue3LayoutParams *)params
                                                                  cellSize:(CGSize)size;

@end
