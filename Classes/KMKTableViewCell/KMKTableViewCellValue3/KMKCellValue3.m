//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellValue3.h"

#import "NSString+KMKUtils.h"

#import "KMKTableViewCellValue3+Subclass.h"


@implementation KMKCellValue3

+ (CGFloat)preferredHeightForDataSource:(KMKTableViewCellValue3LayoutParams *)params width:(CGFloat)width {
    // preferred height doesn't depends on height nevertherless we set CGFLOAT_MAX
    const CGSize kCellSize = CGSizeMake(width, CGFLOAT_MAX);
    KMKTableViewCellValue3LayoutInfo layoutInfo = [[self class] tableViewCellValue3LayoutInfoForParams:params
                                                                                              cellSize:kCellSize];
    return layoutInfo.preferredHeight;
}

#pragma mark
#pragma mark Allocation

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithCellStyle:KMKTableViewCellStyleValue1 reuseIdentifier:reuseIdentifier layoutMaker:nil]) {
        self.clipsToBounds = YES;
        
        [self applyLayoutParams:[KMKTableViewCellValue3LayoutParams defaultLaoutParams]];
        
        self.detailTextLabel.textAlignment = NSTextAlignmentLeft;
        self.detailTextLabel.textColor = [UIColor blackColor];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [self initWithReuseIdentifier:nil];
    if (self) {
        self.frame = frame;
    }
    return self;
}

- (id)init {
    self = [self initWithReuseIdentifier:nil];
    if (self) {
    }
    return self;
}

#pragma mark 
#pragma mark Public

- (void)applyLayoutParams:(KMKTableViewCellValue3LayoutParams *)layoutParams {
    self.textLabel.text = layoutParams.text;
    self.textLabel.numberOfLines = layoutParams.textNumberOfLines;
    self.textLabel.font = layoutParams.textFont;
    self.detailTextLabel.text = layoutParams.detailText;
    self.detailTextLabel.font = layoutParams.detailTextFont;
    self.detailTextLabel.numberOfLines = layoutParams.detailTextNumberOfLines;
    self.imageView.image = layoutParams.image;
    self.accessoryType = layoutParams.accessoryType;
    self.accessoryView = layoutParams.accessoryView;
}

#pragma mark Laying out Subviews

- (CGSize)sizeThatFits:(CGSize)size {
    KMKTableViewCellValue3LayoutInfo layoutInfo = [[self class] tableViewCellValue3LayoutInfoForParams:[self layoutParams]
                                                                                              cellSize:size];
    CGSize resultSize = CGSizeMake(size.width, layoutInfo.preferredHeight);
    return resultSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // debug
//    self.bkgColor = [UIColor blueColor];
//    self.imageView.backgroundColor = [UIColor redColor];
//    self.textLabel.backgroundColor = [UIColor greenColor];
//    self.detailTextLabel.backgroundColor = [UIColor redColor];
    
    KMKTableViewCellValue3LayoutInfo layoutInfo = [[self class] tableViewCellValue3LayoutInfoForParams:[self layoutParams]
                                                                                              cellSize:self.bounds.size];
    self.imageView.frame = layoutInfo.imageFrame;
    self.textLabel.frame = layoutInfo.textFrame;
    self.detailTextLabel.frame = layoutInfo.detailTextFrame;    
}

#pragma mark 
#pragma mark Protected

/**
 Если высоты не достаточно для размещения контента, то контент вертикально выравнивается по верхнему краю (KMKSubtitleCellContentVerticalAlignmentTop).
 */
+ (KMKTableViewCellValue3LayoutInfo)tableViewCellValue3LayoutInfoForParams:(KMKTableViewCellValue3LayoutParams *)params
                                                                  cellSize:(CGSize)size {
    KMKTableViewCellValue3LayoutInfo layoutInfo;
    
    const CGFloat kCellLeftMargin = KMKCellLeftMargin;
    const CGFloat kCellRightMargin = KMKCellRightMargin;
    const CGFloat kCellTopMargin = KMKCellTopMargin;
    const CGFloat kCellBottomMargin = KMKCellBottomMargin;
    const CGFloat kCellHorizontalSpacing = KMKCellHorizontalSpacing;
    
    const CGFloat kImageTopMargin = KMKCellTopMargin;
    const CGFloat kImageBottomMargin = KMKCellBottomMargin;
    
    // image
    CGRect imageFrame = CGRectZero;
    imageFrame.origin.x = kCellLeftMargin;
    imageFrame.size = [params image].size;
    imageFrame.origin.y = floorf((size.height - imageFrame.size.height)/2.0);
    layoutInfo.imageFrame = imageFrame;
    
    // constants for labels
    const CGFloat kOriginXOfFirstLabel = imageFrame.origin.x + imageFrame.size.width + (params.image ? kCellHorizontalSpacing : 0.0);
    const CGFloat kWidthOfFirstLabel = floorf((size.width - kOriginXOfFirstLabel - kCellRightMargin - kCellHorizontalSpacing)/2.0);
    const CGFloat kOriginXOfSecondLabel = kOriginXOfFirstLabel + kWidthOfFirstLabel + kCellHorizontalSpacing;
    const CGFloat kWidthOfSecondLabel = size.width - kOriginXOfSecondLabel - kCellRightMargin;
    
    // textLabel
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = kOriginXOfFirstLabel;
    textFrame.origin.y = kCellTopMargin;
    textFrame.size.width = kWidthOfFirstLabel;
    textFrame.size.height = [params.text kmk_multilineSizeConstrainedToWidth:textFrame.size.width
                                                               numberOfLines:params.textNumberOfLines
                                                                        font:params.textFont
                                                               lineBreakMode:params.textLineBreakMode].height;
    // detailTextLabel
    CGRect detailTextFrame = CGRectZero;
    detailTextFrame.origin.x = kOriginXOfSecondLabel;
    detailTextFrame.origin.y = kCellTopMargin;
    detailTextFrame.size.width = kWidthOfSecondLabel;
    detailTextFrame.size.height = [params.detailText kmk_multilineSizeConstrainedToWidth:detailTextFrame.size.width
                                       numberOfLines:params.detailTextNumberOfLines
                                                font:params.detailTextFont
                                       lineBreakMode:params.detailTextLineBreakMode].height;
    
    // calculating preferred height
    if (params.image == nil && [params.text length] == 0 && [params.detailText length] == 0) {
        layoutInfo.preferredHeight = 0.0;
    }
    else {
        layoutInfo.preferredHeight = MAX(kImageTopMargin + imageFrame.size.height + kImageBottomMargin,
                                         kCellTopMargin + kCellBottomMargin + MAX(textFrame.size.height, detailTextFrame.size.height));
    }
    
    layoutInfo.textFrame = textFrame;
    layoutInfo.detailTextFrame = detailTextFrame;
    
    return layoutInfo;
}

#pragma mark
#pragma mark Private

- (KMKTableViewCellValue3LayoutParams *)layoutParams {
    KMKTableViewCellValue3LayoutParams *layoutParams = [KMKTableViewCellValue3LayoutParams new];
    layoutParams.text = self.textLabel.text;
    layoutParams.textNumberOfLines = self.textLabel.numberOfLines;
    layoutParams.textFont = self.textLabel.font;
    layoutParams.detailText = self.detailTextLabel.text;
    layoutParams.detailTextFont = self.detailTextLabel.font;
    layoutParams.detailTextNumberOfLines = self.detailTextLabel.numberOfLines;
    layoutParams.image = self.imageView.image;
    layoutParams.accessoryType = self.accessoryType;
    layoutParams.accessoryView = self.accessoryView;
    return layoutParams;
}

@end
