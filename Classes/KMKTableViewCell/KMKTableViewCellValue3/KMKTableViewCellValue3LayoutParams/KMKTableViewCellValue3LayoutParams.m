//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKTableViewCellValue3LayoutParams.h"



@implementation KMKTableViewCellValue3LayoutParams

+ (KMKTableViewCellValue3LayoutParams *)defaultLaoutParams {
    KMKTableViewCellValue3LayoutParams *layoutParams = [KMKTableViewCellValue3LayoutParams new];
    return layoutParams;
}

- (id)init {
    if (self = [super init]) {
        self.textNumberOfLines = 1;
        self.textFont = [UIFont systemFontOfSize:17.0];        
        self.detailTextNumberOfLines = 1;
        self.detailTextFont = [UIFont systemFontOfSize:14.0];
        self.accessoryType = UITableViewCellAccessoryNone;
    }
    return self;
}

#pragma mark NSCopying

- (id)copyWithZone:(NSZone *)zone {
    KMKTableViewCellValue3LayoutParams *layoutParams;
    layoutParams = [[self class] allocWithZone:zone];
    layoutParams.text = self.text;
    layoutParams.textFont = self.textFont;
    layoutParams.textNumberOfLines = self.textNumberOfLines;
    layoutParams.detailText = self.detailText;
    layoutParams.detailTextFont = self.detailTextFont;
    layoutParams.detailTextNumberOfLines = self.detailTextNumberOfLines;
    layoutParams.image = self.image;
    layoutParams.accessoryView = self.accessoryView;
    layoutParams.accessoryType = self.accessoryType;
    return layoutParams;
}

@end
