//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>


@interface KMKTableViewCellValue3LayoutParams : NSObject <NSCopying>

@property (strong, nonatomic) NSString *text;
@property (nonatomic, assign) NSInteger textNumberOfLines;
@property (strong, nonatomic) UIFont *textFont;

/**
 lineBreakMode of textLabel.
 */
@property (nonatomic, assign) NSLineBreakMode textLineBreakMode;

@property (strong, nonatomic) NSString *detailText;
@property (nonatomic, assign) NSInteger detailTextNumberOfLines;
@property (strong, nonatomic) UIFont *detailTextFont;

/**
 lineBreakMode of detailTextLabel.
 */
@property (nonatomic, assign) NSLineBreakMode detailTextLineBreakMode;

@property (strong, nonatomic) UIImage *image;
@property (nonatomic, assign) UITableViewCellAccessoryType accessoryType;
@property (strong, nonatomic) UIView *accessoryView;

/**
 Initializes properties with default values.
 */
- (id)init;

/**
 @return Object with defautl values of parameters.
 */
+ (KMKTableViewCellValue3LayoutParams *)defaultLaoutParams;

@end
