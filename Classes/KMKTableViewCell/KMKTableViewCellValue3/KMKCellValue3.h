//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

#import "KMKTableViewCellValue3LayoutParams.h"

/**
 Ячейка содержит две метки, располагаемые одна справа, вторая - слева.
 - Текст меток по умолчанию выравнен по левому краю;
 - Рисунок всегда выравнивается по центру по вертикали;
 - Ширина делится поровну между метками
 */
@interface KMKCellValue3 : KMKCell

/**
 Designated initilizer.
 */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)initWithFrame:(CGRect)frame;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)init;

/**
 Number of lines is taken into account when calculating height.
 
 - if there is no content then result is 0.0;
 */
+ (CGFloat)preferredHeightForDataSource:(KMKTableViewCellValue3LayoutParams *)params width:(CGFloat)width;

/**
 Apply specified params @code{layoutParams} to cell.
 */
- (void)applyLayoutParams:(KMKTableViewCellValue3LayoutParams *)layoutParams;

/**
 Retrurns layout parameters of cell.
 Layout parameters are usually used to determine height of the cell.
 */
- (KMKTableViewCellValue3LayoutParams *)layoutParams;

@end
