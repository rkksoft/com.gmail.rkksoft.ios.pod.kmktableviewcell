//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

// used for image or for label
static const CGFloat KMKCellLeftMargin = 10.0;
// used for image or for label
static const CGFloat KMKCellTopMargin = 5.0;
// used for image or for label
static const CGFloat KMKCellBottomMargin = 5.0;
// Showing up sroll bar is taken into account. When value is 5 text can intersect with scroll bar.
static const CGFloat KMKCellRightMargin = 8.0;
// The space between labels placed side by side vertically.
static const CGFloat KMKCellVerticalSpacing = 3.0;
// The space between labels placed side by side horizontally. Can be used for spacing between label and image.
static const CGFloat KMKCellHorizontalSpacing = 10.0;
static const CGFloat KMKCellSeparatorHeight = 1.0;


@interface KMKCellConstants : NSObject


@end
