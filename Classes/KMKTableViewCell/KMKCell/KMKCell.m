//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

//#import "UIImage+KMKUtils.h"
#import "KMKCellLayoutParamsImpl.h"
#import "KMKCell+Subclass.h"
#import "KMKCellLayoutInfoImpl.h"
#import "KMKCellLayoutMaker.h"
#import "KMKCellConstants.h"

#import "KMKCellStyleDefaultLayoutMaker.h"
#import "KMKCellStyleSubtitleLayoutMaker.h"


@interface KMKCell ()

/**
 Object responsible for making layout. 
 Version of object depends on the cell style.
 */
@property (strong, nonatomic) id<KMKCellLayoutMaker> layoutMaker;

@end


@implementation KMKCell

#pragma mark
#pragma mark Class methods

// TODO: implemented only for style KMKTableViewCellStyleDefault
// TODO: replace preferredHeightForDataSource with preferredSizeForDataSource
+ (CGFloat)preferredHeightForDataSource:(id<KMKCellLayoutParams>)params width:(CGFloat)width {
    id<KMKCellLayoutMaker> layoutMaker = [self defaultLayoutMakerForCellStyle:params.cellStyle];
    KMKCellLayoutInfoImpl *layoutInfo = [layoutMaker layoutInfoForParams:params
                                                                cellSize:CGSizeMake(width, CGFLOAT_MAX)];
    return layoutInfo.contingentRightSize.height;
}

#pragma mark
#pragma mark Allocation

// designated initializer
- (id)initWithCellStyle:(KMKTableViewCellStyle)cellStyle
        reuseIdentifier:(NSString *)reuseIdentifier
            layoutMaker:(id<KMKCellLayoutMaker>)layoutMaker {
    // temporary decision while not all style are implemented
    UITableViewCellStyle style = UITableViewCellStyleDefault;
    if (cellStyle == KMKTableViewCellStyleValue1) {
        style = UITableViewCellStyleValue1;
    }
    else if (cellStyle == KMKTableViewCellStyleValue2) {
        style = UITableViewCellStyleValue2;
    }
    else if (cellStyle == KMKTableViewCellStyleSubtitle) {
        style = UITableViewCellStyleSubtitle;
    }
    else {
        // leave default value UITableViewCellStyleDefault
    }
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        if (layoutMaker != nil) {
            self.layoutMaker = layoutMaker;
        }
        else {
            self.layoutMaker = [[self class] defaultLayoutMakerForCellStyle:cellStyle];
        }
        
        _cellStyle = cellStyle;
        [self init_common_cell];
    }
    return self;
}

- (id)initWithCellStyle:(KMKTableViewCellStyle)cellStyle reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [self initWithCellStyle:cellStyle
                       reuseIdentifier:reuseIdentifier
                           layoutMaker:nil]) {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self init_common_cell];
    }
    return self;
}

- (void)init_common_cell {
    self.bkgColor = [UIColor whiteColor];
    self.customSeparatorColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    self.backgroundColor = [UIColor clearColor]; // in case when cell is used outside table background color is nil by default what leds to black color for background
}

#pragma mark UIView's methods

// line sepatator is drawn at the bottom
- (void)drawRect:(CGRect)rect {
	[super drawRect:rect];
    
	if (self.backgroundView == nil) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        CGColorRef CGColor = [self.bkgColor CGColor];
        CGContextSetFillColorWithColor(context, CGColor);
        
        CGRect frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
        CGContextFillRect(context, frame);
        
        CGColorRef separatorCGColor = [self.customSeparatorColor CGColor];
        CGContextSetFillColorWithColor(context, separatorCGColor);
        CGRect line = CGRectMake(0, rect.size.height - KMKCellSeparatorHeight, rect.size.width, KMKCellSeparatorHeight);
        CGContextFillRect(context, line);
    }
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize bestSize = [super sizeThatFits:size];
    
    if (self.layoutMaker != nil) { // some styles are not implemented and there are no default maker for them
        KMKCellLayoutInfoImpl *layoutInfo = [self.layoutMaker layoutInfoForParams:[self layoutParams]
                                                                         cellSize:self.bounds.size];
        bestSize = CGSizeMake(size.width, layoutInfo.contingentRightSize.height);
    }
    return bestSize;
}

// If style is KMKTableViewCellStyleNone then layout is not perfomed. It allow to avoid computational burden when subclass
// invoke super's implementation and the result of super's work is not used.
- (void)layoutSubviews {
    [super layoutSubviews];
    // debug
//    self.textLabel.backgroundColor = [UIColor greenColor];
    
    if (self.layoutMaker != nil) { // some styles are not implemented and there are no default maker for them
        KMKCellLayoutInfoImpl *layoutInfo = [self.layoutMaker layoutInfoForParams:[self layoutParams]
                                                                         cellSize:self.bounds.size];
        [self applyLayoutInfo:layoutInfo];
    }
}

#pragma mark
#pragma mark Public

- (id<KMKCellLayoutParams>)layoutParams {
    KMKCellLayoutParamsImpl *layoutParams = [KMKCellLayoutParamsImpl new];
    layoutParams.cellStyle = self.cellStyle;
    
    layoutParams.attributedText = self.textLabel.attributedText;
    //TODO: remove this line after full migrating to support of attributed text (Default style is in question)
    layoutParams.text = self.textLabel.text;
    layoutParams.textNumberOfLines = self.textLabel.numberOfLines;
    //TODO: remove this line after full migrating to support of attributed text (Default style is in question)
    layoutParams.textFont = self.textLabel.font;
    layoutParams.textLineBreakMode = self.textLabel.lineBreakMode;
    
    layoutParams.detailAttributedText = self.detailTextLabel.attributedText;
    layoutParams.textNumberOfLines = self.detailTextLabel.numberOfLines;
    
    layoutParams.accessoryType = self.accessoryType;
    layoutParams.accessoryView = self.accessoryView;
    return layoutParams;
}

#pragma mark
#pragma mark Protected

/**
 Perfom applying layout description from KMKCellLayoutInfo to the cell.
 Method is invoked by layoutSubviews method.
 Subclasses may use their own implementation of KMKCellLayoutInfo and may use additional attributes.
 It is not obligatory to invoke super in subclasses.
 */
- (void)applyLayoutInfo:(id<KMKCellLayoutInfo>)layoutInfo {
    self.imageView.frame = layoutInfo.imageFrame;
    self.textLabel.frame = layoutInfo.textFrame;
    self.detailTextLabel.frame = layoutInfo.detailTextFrame;
}

// For the style KMKTableViewCellStyleNone returns nil. See comment for KMKTableViewCellStyleNone value.
// But subclasses may return some maker for KMKTableViewCellStyleNone.
+ (id<KMKCellLayoutMaker>)defaultLayoutMakerForCellStyle:(KMKTableViewCellStyle)cellStyle {
    static NSDictionary *cellStyleToLayoutMaker = nil;
    if (cellStyleToLayoutMaker == nil) {
        
        cellStyleToLayoutMaker = @{
                                   @(KMKTableViewCellStyleDefault) : [KMKCellStyleDefaultLayoutMaker layoutMaker],
                                   @(KMKTableViewCellStyleSubtitle) : [KMKCellStyleSubtitleLayoutMaker layoutMaker],
          };
    }
    
    id<KMKCellLayoutMaker> layoutMaker = [cellStyleToLayoutMaker objectForKey:@(cellStyle)];
    
    return layoutMaker;
}

@end
