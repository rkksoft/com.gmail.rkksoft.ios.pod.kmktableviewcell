//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutParamsImpl.h"


@implementation KMKCellLayoutParamsImpl

static UILabel *sModelBehaviorLabel = nil;

+ (void)initialize {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sModelBehaviorLabel = [UILabel new];
    });
}

+ (id<KMKCellLayoutParams>)layoutParams {
    return [[[self class] alloc] init];
}

+ (id<KMKCellLayoutParams>)layoutParamsWithLayoutParams:(id<KMKCellLayoutParams>)params {
    KMKCellLayoutParamsImpl *layoutParams = [[self class] new]; // polymorphous pointer
    layoutParams.cellStyle = params.cellStyle;
    
    layoutParams.text = params.text;
    layoutParams.textNumberOfLines = params.textNumberOfLines;
    layoutParams.textFont = params.textFont;
    layoutParams.textLineBreakMode = params.textLineBreakMode;
    
    layoutParams.detailText = params.detailText;
    layoutParams.detailTextNumberOfLines = params.detailTextNumberOfLines;
    layoutParams.detailTextFont = params.detailTextFont;
    layoutParams.detailTextLineBreakMode = params.detailTextLineBreakMode;
    
    layoutParams.image = params.image;
    layoutParams.accessoryType = params.accessoryType;
    layoutParams.accessoryView = params.accessoryView;
    return layoutParams;
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    _attributedText = attributedText;
    sModelBehaviorLabel.attributedText = attributedText;
    _textFont = sModelBehaviorLabel.font;
    _textLineBreakMode = sModelBehaviorLabel.lineBreakMode;
}

- (void)setTextFont:(UIFont *)textFont {
    _textFont = textFont;
    sModelBehaviorLabel.attributedText = _attributedText;
    sModelBehaviorLabel.font = textFont;
    _attributedText = sModelBehaviorLabel.attributedText;
}

- (void)setTextLineBreakMode:(NSLineBreakMode)textLineBreakMode {
    _textLineBreakMode = textLineBreakMode;
    sModelBehaviorLabel.attributedText = _attributedText;
    sModelBehaviorLabel.lineBreakMode = textLineBreakMode;
    _attributedText = sModelBehaviorLabel.attributedText;
}

- (void)setDetailAttributedText:(NSAttributedString *)attributedText {
    _detailAttributedText = attributedText;
    sModelBehaviorLabel.attributedText = attributedText;
    _detailTextFont = sModelBehaviorLabel.font;
    _detailTextLineBreakMode = sModelBehaviorLabel.lineBreakMode;
}

- (void)setDetailTextFont:(UIFont *)detailTextFont {
    _detailTextFont = detailTextFont;
    sModelBehaviorLabel.attributedText = _attributedText;
    sModelBehaviorLabel.font = detailTextFont;
    _detailAttributedText = sModelBehaviorLabel.attributedText;
}

- (void)setDetailTextLineBreakMode:(NSLineBreakMode)detailTextLineBreakMode {
    _detailTextLineBreakMode = detailTextLineBreakMode;
    sModelBehaviorLabel.attributedText = _attributedText;
    sModelBehaviorLabel.lineBreakMode = detailTextLineBreakMode;
    _detailAttributedText = sModelBehaviorLabel.attributedText;
}

@end
