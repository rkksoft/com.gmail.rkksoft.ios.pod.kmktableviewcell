//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellStyleSubtitleLayoutMaker.h"

#import "KMKCellConstants.h"
#import "KMKCellLayoutInfoImpl.h"
#import "KMKCellStyleDefaultLayoutMaker.h"
#import "KMKCell+Utils.h"
#import "KMKCellLayoutParamsImpl.h"
#import "NSString+KMKUtils.h"
#import "NSAttributedString+KMKUtils.h"


@implementation KMKCellStyleSubtitleLayoutMaker

- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size {
    
    KMKCellLayoutParamsImpl *paramsSubtitle = (KMKCellLayoutParamsImpl *)params;
    
    KMKCellLayoutInfoImpl *layoutInfo = [[KMKCellLayoutInfoImpl alloc] init];
    
    const CGFloat kImageTopMargin = KMKCellTopMargin;
    const CGFloat kImageBottomMargin = KMKCellBottomMargin;
    
    // image
    CGRect imageFrame = CGRectZero;
    imageFrame.origin.x = KMKCellLeftMargin;
    imageFrame.size = [paramsSubtitle image].size;
    imageFrame.origin.y = floorf((size.height - imageFrame.size.height)/2.0);
    layoutInfo.imageFrame = imageFrame;
    
    // textLabel
    const CGFloat kOriginXOfLabels = imageFrame.origin.x + imageFrame.size.width + (paramsSubtitle.image ? KMKCellHorizontalSpacing : 0.0);
    const CGFloat kWidthOfLabels = size.width - kOriginXOfLabels - [KMKCell rightMarginForLayoutParams:paramsSubtitle];
    
    // textLabel
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = kOriginXOfLabels;
    textFrame.origin.y = KMKCellTopMargin;
    textFrame.size.width = kWidthOfLabels;
    textFrame.size.height = [paramsSubtitle.attributedText kmk_multilineSizeConstrainedToWidth:size.width
                                                                                 numberOfLines:paramsSubtitle.textNumberOfLines].height;
    
    // detailTextLabel
    CGRect detailTextFrame = CGRectZero;
    detailTextFrame.origin.x = kOriginXOfLabels;
    detailTextFrame.origin.y = textFrame.origin.y + textFrame.size.height;
    detailTextFrame.size.width = kWidthOfLabels;
    detailTextFrame.size.height = [paramsSubtitle.detailAttributedText kmk_multilineSizeConstrainedToWidth:size.width
                                                                                             numberOfLines:paramsSubtitle.detailTextNumberOfLines].height;
    
    // calculating contingent right size (preferred height)
    CGFloat congingentRightHeght = 0.0;
    const CGFloat kPreferredHeightForLabels = KMKCellTopMargin + KMKCellBottomMargin + (CGRectGetMaxY(detailTextFrame) - textFrame.origin.y);
    
    if (paramsSubtitle.image == nil &&
        [paramsSubtitle.text length] == 0 &&
        [paramsSubtitle.detailText length] == 0) {
        
        congingentRightHeght = 0.0;
    }
    else {
        congingentRightHeght = MAX(kImageTopMargin + imageFrame.size.height + kImageBottomMargin, kPreferredHeightForLabels);
    }
    layoutInfo.contingentRightSize = CGSizeMake(size.width, congingentRightHeght);
    
    
    // Centering and cutting labels if needed
    CGFloat dY = 0.0;
//    // TODO: обработана только ситуация, когда detailTextLabel не вмещается. Еще надо обработать textLabel и thirdTextLabel.
//    if (kPreferredHeightForLabels > size.height) { // if height is not large enough to fit labels
//        // If detailTextLabel doesn't fit into cell taking into account top and bottom margins
//        if (detailTextFrame.origin.y + detailTextFrame.size.height + KMKCellBottomMargin > size.height) {
//            NSInteger detailNumberOfLines = floorf((size.height - detailTextFrame.origin.y - KMKCellBottomMargin)/[[paramsSubtitle detailTextFont] lineHeight]);
//            if (detailNumberOfLines >= 0) {
//                detailTextFrame.size.height = [UILabel kmk_sizeForText:cellLayoutParamsSubtitle.thirdText
//                                                                 width:kWidthOfLabels
//                                                         numberOfLines:cellLayoutParamsSubtitle.thirdTextNumberOfLines
//                                                                  font:cellLayoutParamsSubtitle.textFont
//                                                         lineBreakMode:cellLayoutParamsSubtitle.textLineBreakMode].height;
//                dY = floorf((size.height - detailTextFrame.origin.y - detailTextFrame.size.height - KMKCellBottomMargin)/2.0);
//            }
//            else {
//                //TODO: ситуация, когда textLabel не вмещается, не обработана
//                dY = 0;
//            }
//        }
//    }
//    else {
    dY = floorf((size.height - detailTextFrame.origin.y - detailTextFrame.size.height - KMKCellBottomMargin)/2.0);

//    }
    
    textFrame = CGRectOffset(textFrame, 0.0, dY);
    detailTextFrame = CGRectOffset(detailTextFrame, 0.0, dY);
    
    layoutInfo.textFrame = textFrame;
    layoutInfo.detailTextFrame = detailTextFrame;

    return layoutInfo;
}

@end
