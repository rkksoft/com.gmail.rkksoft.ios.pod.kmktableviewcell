//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutMakerImpl.h"


@interface KMKCellStyleSubtitleLayoutMaker : KMKCellLayoutMakerImpl

@end
