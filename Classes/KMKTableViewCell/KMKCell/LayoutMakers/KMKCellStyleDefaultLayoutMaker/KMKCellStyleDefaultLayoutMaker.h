//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutMakerImpl.h"


/**
 Layout maker for `KMKTableViewCellStyleDefault` style of cell.
 Width of cell is left the same.
 */
@interface KMKCellStyleDefaultLayoutMaker : KMKCellLayoutMakerImpl

@end
