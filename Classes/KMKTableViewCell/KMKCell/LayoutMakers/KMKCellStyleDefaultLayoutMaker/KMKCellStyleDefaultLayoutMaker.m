//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellStyleDefaultLayoutMaker.h"

#import "KMKCellConstants.h"
#import "KMKCellLayoutInfoImpl.h"
#import "KMKCellStyleDefaultLayoutMaker.h"
#import "KMKCell+Utils.h"
#import "KMKCellLayoutParamsImpl.h"
#import "NSString+KMKUtils.h"


@implementation KMKCellStyleDefaultLayoutMaker

- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size {
    
    KMKCellLayoutParamsImpl *paramsDefault = (KMKCellLayoutParamsImpl *)params;
    
    KMKCellLayoutInfoImpl *layoutInfo = [[KMKCellLayoutInfoImpl alloc] init];
    
    const CGFloat kImageTopMargin = KMKCellTopMargin;
    const CGFloat kImageBottomMargin = KMKCellBottomMargin;
    
    CGRect imageFrame = CGRectZero;
    imageFrame.size = [paramsDefault image].size;
    imageFrame.origin.y = floorf((size.height - imageFrame.size.height)/2.0);
    imageFrame.origin.x = KMKCellLeftMargin;
    layoutInfo.imageFrame = imageFrame;
    
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = imageFrame.origin.x + imageFrame.size.width + (paramsDefault.image ? KMKCellHorizontalSpacing : 0.0);
    textFrame.size.width = size.width - textFrame.origin.x - [KMKCell rightMarginForLayoutParams:paramsDefault];
    
    CGSize textSize = [paramsDefault.text kmk_multilineSizeConstrainedToWidth:size.width
                                                                numberOfLines:paramsDefault.textNumberOfLines
                                                                         font:paramsDefault.textFont
                                                                lineBreakMode:paramsDefault.textLineBreakMode];
    
    textFrame.origin.y = floorf((size.height - textSize.height)/2.0);
    textFrame.size.height = textSize.height;
    layoutInfo.textFrame = textFrame;
    
    CGFloat preferredHeight = 0.0;
    if ([paramsDefault.text length] != 0) {
        preferredHeight = MAX(kImageTopMargin + imageFrame.size.height + kImageBottomMargin,
                              KMKCellTopMargin + textSize.height + KMKCellBottomMargin);
    }
    
    CGFloat contingentRithtHeight = MIN(preferredHeight, size.height);
    layoutInfo.contingentRightSize = CGSizeMake(size.width, contingentRithtHeight);
    return layoutInfo;
}

@end
