//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

#import "KMKCellLayoutParams.h"
#import "KMKCell+Utils.h"
#import "KMKCellConstants.h"
#import "KMKCellLayoutMaker.h"


// Description of layout.
typedef struct {
    CGRect textFrame;
    CGRect imageFrame;
    CGFloat preferredHeight;
} KMKCellLayoutInfo;


@interface KMKCell (Subclass)

/**
 Perfom applying layout description from KMKCellLayoutInfo to the cell.
 Method is invoked by layoutSubviews method.
 Subclasses may use their own implementation of KMKCellLayoutInfo and may use additional attributes.
 It is not obligatory to invoke super in subclasses.
 */
- (void)applyLayoutInfo:(id<KMKCellLayoutInfo>)layoutInfo;

// Subclass may overide this method to return it's own layout maker.
+ (id<KMKCellLayoutMaker>)defaultLayoutMakerForCellStyle:(KMKTableViewCellStyle)cellStyle;

@end
