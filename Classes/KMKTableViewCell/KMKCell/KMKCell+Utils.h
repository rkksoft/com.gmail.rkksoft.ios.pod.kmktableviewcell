//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

/**
 Creating of this category allowed to reduce the size of KMKCell.m 
 Moving code to the category "Subclass" is not available because this category serves only for declaration of methods 
 that may be overriden in subclasses but default implementation contains in the implementatin section of the class itself.
 */
@interface KMKCell (Utils)

/**
 Right margin for labels taking into account existance of accessory view.
 */
+ (CGFloat)rightMarginForLayoutParams:(id<KMKCellLayoutParams>)layoutParams;

@end
