//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutInfo.h"
#import "KMKCellLayoutParams.h"


/**
 Interface for making layout algorithm.
 (Pattern "Strategy").
 */
@protocol KMKCellLayoutMaker <NSObject>

- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size;

@end
