//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutParams.h"
#import "KMKCellLayoutInfo.h"
#import "KMKCellLayoutMaker.h"


/**
 Targets:
 - add separator line at the bottom of cell. This allows us not to use separates of table. Otherwise if the table has the height less than screen height, then in area not filled with cells will be displayed separates. If background != nil, then separator isn't shown.
 -  allow to calculate height of the cell without creating it
 */
@interface KMKCell : UITableViewCell {
    UIColor *_color;
}

#pragma mark 
#pragma mark Class methods

/**
 Number of lines is taken into account when calculating height.
 Fot calculating the preferred height is used default layout maker.
 */
// TODO: add support of passing custom layout maker.
+ (CGFloat)preferredHeightForDataSource:(id<KMKCellLayoutParams>)params width:(CGFloat)width;

#pragma mark

/**
 IMPORTANT: The new name is slightly different from the name of the designated initializer of the standard class
 UITableViewCell. Do not confuse them.
 */
- (id)initWithCellStyle:(KMKTableViewCellStyle)cellStyle reuseIdentifier:(NSString *)reuseIdentifier;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier NS_UNAVAILABLE;

/**
 Designated initializer.
 @param layoutMaker Algorithm of making layout. May be nil, then default layout meker is used.
 */
- (id)initWithCellStyle:(KMKTableViewCellStyle)cellStyle
        reuseIdentifier:(NSString *)reuseIdentifier
            layoutMaker:(id<KMKCellLayoutMaker>)layoutMaker;

/**
 Cell style. 
 The name has chosen such to avoid name conflict with possible private instance variable "style".
 */
@property (nonatomic, readonly) KMKTableViewCellStyle cellStyle;

/**
 Bachground color of cell. Property backroundColor is impossibe to use, as it is set by framework into clear color before invoking layoutSubviews and drawRect.
 
 Note: Above mentioned behaviour is observed only in case when cell is in table. Oherwise the color is clear, what is equivalent to black color.
 */
@property (strong, nonatomic) UIColor *bkgColor;

/**
 Color of separator line.
 The name separatorColor is used in UITableViewCell as name for private property.
 Default value 90% gray.
 If backgroundView is not nil then separator is not drawn.
 */
@property (strong, nonatomic) UIColor *customSeparatorColor;

/**
 Retrurns layout parameters of cell.
 Layout parameters are usually used to determine height of the cell not creating cell itself. When determining the height
 KMKCellLayoutParams is passed to certain class method.
 */
- (id<KMKCellLayoutParams>)layoutParams;

@end
