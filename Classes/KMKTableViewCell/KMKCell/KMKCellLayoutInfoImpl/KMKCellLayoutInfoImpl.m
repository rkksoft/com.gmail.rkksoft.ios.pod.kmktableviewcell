//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutInfoImpl.h"


@implementation KMKCellLayoutInfoImpl

+ (id<KMKCellLayoutInfo>)layoutInfo {
    return [[[self class] alloc] init];
}

@end
