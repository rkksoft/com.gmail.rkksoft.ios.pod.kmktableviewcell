//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutInfo.h"


@interface KMKCellLayoutInfoImpl : NSObject <KMKCellLayoutInfo>

/**
 Create and returns instance of the class.
 */
+ (id<KMKCellLayoutInfo>)layoutInfo;

@property (nonatomic, assign) CGRect textFrame;
@property (nonatomic, assign) CGRect detailTextFrame;
@property (nonatomic, assign) CGRect imageFrame;
@property (nonatomic, assign) CGSize contingentRightSize;

@end
