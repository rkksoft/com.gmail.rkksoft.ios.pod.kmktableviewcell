//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell+Utils.h"

#import "KMKCellConstants.h"


@implementation KMKCell (Utils)

#pragma mark
#pragma mark Public

+ (CGFloat)rightMarginForLayoutParams:(id<KMKCellLayoutParams>)layoutParams {
    CGFloat rightMargin = 0.0;
    if (layoutParams.accessoryView != nil) {
        rightMargin = [KMKCell _rightMarginForCellLbsWithAccessoryView:layoutParams.accessoryView];
    }
    else {
        rightMargin = [KMKCell _rightMarginForAccessoryType:layoutParams.accessoryType];
    }
    return rightMargin;
}

#pragma mark
#pragma mark Private

/**
 Return value of the right margin of labels in case of accessary view of specified type
 It is used for proper layout labels of cell (ignoring this aspect can clead to displaying text over accessory view).
 */
+ (CGFloat)_rightMarginForAccessoryType:(UITableViewCellAccessoryType)accessoryType {
    CGFloat result = 0;
    
    switch (accessoryType) {
        case UITableViewCellAccessoryNone:
            result = KMKCellRightMargin;
            break;
        case UITableViewCellAccessoryDisclosureIndicator:
            result = 25.0;
            break;
        default:
            NSAssert (NO, @"Not implemented!");
            break;
    }
    return result;
}


/**
 Return value of the right margin of labels in case of specified accessoryView
  It is used for proper layout labels of cell (ignoring this aspect can lead to displaying text over accessory view).
 */
+ (CGFloat)_rightMarginForCellLbsWithAccessoryView:(UIView *)view {
    const CGFloat kRightMarginForAccessoryView = 17.0;
    const CGFloat kSpaceBetweenContentAndAccessoryView = 5.0;
    CGFloat result = kSpaceBetweenContentAndAccessoryView + view.frame.size.width + kRightMarginForAccessoryView;
    return result;
}

@end
