//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutMakerImpl.h"

@interface KMKCellLayoutMakerImpl (Subclass)

/**
 Left margin of the cell. 
 Set via setLeftMargin: method of KMKCell.
 */
@property (nonatomic, assign) CGFloat leftMargin;

@end
