//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutMakerImpl.h"

#import "KMKCellConstants.h"


@interface KMKCellLayoutMakerImpl ()

@property (nonatomic, assign) CGFloat leftMargin;

@end


@implementation KMKCellLayoutMakerImpl

#pragma mark
#pragma mark Class methods

+ (id<KMKCellLayoutMaker>)layoutMaker {
    return [[[self class] alloc] init];
}

#pragma mark
#pragma mark Initialization

- (id)init {
    if (self = [super init]) {
        _leftMargin = KMKCellLeftMargin;
    }
    return self;
}

#pragma mark 
#pragma mark Public

- (void)setLeftMargin:(CGFloat)leftMargin {
    _leftMargin = leftMargin;
}

#pragma mark <KMKCellLayoutMaker>

- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size {
    NSAssert(NO, @"The Abstract method!");
    return nil;
}

@end
