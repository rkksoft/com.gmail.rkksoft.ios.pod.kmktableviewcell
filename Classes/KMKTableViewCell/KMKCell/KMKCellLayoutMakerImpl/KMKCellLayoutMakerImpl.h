//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutMaker.h"

/**
 Abstract class.
 */
// TODO: to think of the classes (LayoutInfoImpl, LayoutParamsImpl). Why they are not abstract.
@interface KMKCellLayoutMakerImpl : NSObject <KMKCellLayoutMaker>

/**
 Creatate and return instance.
 */
+ (id<KMKCellLayoutMaker>)layoutMaker;

/**
 Set left margin of the cell. Default value is KMKCellLeftMargin.
 */
- (void)setLeftMargin:(CGFloat)leftMargin;

@end
