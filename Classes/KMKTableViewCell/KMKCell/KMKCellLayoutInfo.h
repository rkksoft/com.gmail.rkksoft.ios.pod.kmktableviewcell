//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

/**
 Description of layout.
 Class plays important role in making layout. 
 It is used by method which perfom layout directly. This methods return result of calculatins by using this class.
 Subclasses of KMKCell may use subclasses of this class to achive their targets.
 */
@protocol KMKCellLayoutInfo <NSObject>

@property (nonatomic, assign) CGRect textFrame;
@property (nonatomic, assign) CGRect detailTextFrame;
@property (nonatomic, assign) CGRect imageFrame;

/**
 The size to fit content of view taking into account the specified layout params and max size (contingent).
 */
@property (nonatomic, assign) CGSize contingentRightSize;

@end
