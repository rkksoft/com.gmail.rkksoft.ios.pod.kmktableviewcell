//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellTypes.h"


/**
 Layout params of cell are used to perform laout of cell.
 This class is especially important when it is neccessary to calculate the height of cell but cell itself musn't be 
 created. In this case all params for making layout are specified using KMKCellLayoutParams.
 */
@protocol KMKCellLayoutParams <NSObject>

/**
 Cell style.
 */
@property (nonatomic, readonly, assign) KMKTableViewCellStyle cellStyle;


#pragma mark `textLabel`

/**
 Text of textLabel.
 */
@property (nonatomic, readonly, retain) NSString *text;
@property (strong, readonly, nonatomic) NSAttributedString *attributedText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, readonly, assign) NSInteger textNumberOfLines;

/**
 Font of textLabel.
 */
@property (nonatomic, readonly, retain) UIFont *textFont;

/**
 Line break mode of the textLabel.
 */
@property (nonatomic, readonly, assign) NSLineBreakMode textLineBreakMode;


#pragma mark `detailTextLabel`

/**
 Text of detailTextLabel.
 */
@property (retain, readonly, nonatomic) NSString *detailText;
@property (strong, readonly, nonatomic) NSAttributedString *detailAttributedText;

/**
 Max number of lines in detailTextLabel.
 */
@property (nonatomic, assign) NSInteger detailTextNumberOfLines;

/**
 Font of detailTextLabel.
 */
@property (retain, nonatomic) UIFont *detailTextFont;

/**
 lineBreakMode of detailTextLabel.
 */
@property (nonatomic, assign) NSLineBreakMode detailTextLineBreakMode;


#pragma mark -

/**
 May be nil.
 */
@property (nonatomic, readonly, retain) UIImage *image;

/**
 Accessory type of cell.
 */
@property (nonatomic, readonly, assign) UITableViewCellAccessoryType accessoryType;

/**
 Accessory view of cell.
 */
@property (nonatomic, readonly, retain) UIView *accessoryView;

@end
