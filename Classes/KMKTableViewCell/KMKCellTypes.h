//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

/**
 @abstract Cell style
 @constant KMKTableViewCellStyleNone
    Used mainly in subclasses to avoid computational burden.
    For example when subclass invoke super in layoutSubviews method super's implementatin may do nothing if it discover 
    that style is KMKTableViewCellStyleNone.
 
 @constant KMKTableViewCellStyleDefault == UITableViewCellStyleDefault
 @constant KMKTableViewCellStyleValue1 == UITableViewCellStyleValue1
 @constant KMKTableViewCellStyleValue2 == UITableViewCellStyleValue2
 @constant KMKTableViewCellStyleSubtitle == UITableViewCellStyleSubtitle
 @constant ...
 */
typedef enum {
    KMKTableViewCellStyleNone,
    KMKTableViewCellStyleDefault,
    KMKTableViewCellStyleValue1,
    KMKTableViewCellStyleValue2,
    KMKTableViewCellStyleSubtitle
} KMKTableViewCellStyle;


/**
 Vertical alignemnt of labels in the cell.
 */
typedef enum {
    KMKCellVerticalContentLabelsAlignmentTop = 0,
    KMKCellVerticalContentLabelsAlignmentCenter,
    KMKCellVerticalContentLabelsAlignmentBottom
} KMKCellVerticalContentLabelsAlignment;
