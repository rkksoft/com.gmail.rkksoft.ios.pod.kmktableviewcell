//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutInfoImpl.h"


@interface KMKCellLabels32LayoutInfo : KMKCellLayoutInfoImpl

@property (nonatomic, assign) CGRect imageWithTitleFrame;
@property (nonatomic, assign) CGRect firstPairOfLabelsFrame;
@property (nonatomic, assign) CGRect secondPairOfLabelsFrame;
@property (nonatomic, assign) CGRect thirdPairOfLabelsFrame;

@end
