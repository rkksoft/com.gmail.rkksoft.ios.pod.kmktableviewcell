//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

#import "KMKImageViewWithTitle.h"
#import "KMKHorizPairOfLabelsView.h"
#import "KMKCellTypes.h"


/**
 Used when cells are paired (located in pairs).
 Maximum number of pairs is currently 3.
 Allows also to display small message (max. number of lines 1) under image located in the left side (imageViewWithTitle 
 property).
 */
@interface KMKCellLabels32 : KMKCell

/**
 Title is displayed under the image. Image is displayed on the left - in the same location where common image view is usually located.
 
 Property @code{imageView} is also can be used for showing image,but actually `imageView` property returns `imageView` property of the `imageViewWithTitle`.
 
 `textLabel` has font "system, regular, 14pt". 
 */
@property (nonatomic, readonly) KMKImageViewWithTitle *imageViewWithTitle;

/**
 First label of the second pair
 Font: system, regular, 14pt
 */
@property (nonatomic, readonly) UILabel *text1Label;

/**
 Second label of the second pair
 */
@property (nonatomic, readonly) UILabel *detailText1Label;

/**
 First label of the third pair
 */
@property (nonatomic, readonly) UILabel *text2Label;

/**
 Second label of the third pair
 */
@property (nonatomic, readonly) UILabel *detailText2Label;

/**
 Priority of the first pair of labels (textLabel and detailTextLabel).
 Default value is KMKLabelPairPrioritySecond.
 @see comment for KMKLabelPairPriority.
 */
@property (nonatomic, assign) KMKLabelPairPriority textAndDetailTextPairPriority;

/**
 Priority of the first pair of labels (text1Label and detail1TextLabel).
 Default value is KMKLabelPairPrioritySecond.
 @see comment for KMKLabelPairPriority.
 */
@property (nonatomic, assign) KMKLabelPairPriority text1AndDetailText1PairPriority;

/**
 Priority of the first pair of labels (text2Label and detailText2Label).
 Default value is KMKLabelPairPrioritySecond.
 @see comment for KMKLabelPairPriority.
 */
@property (nonatomic, assign) KMKLabelPairPriority text2AndDetailText2PairPriority;

/**
 Determines vertical alignment of label pairs relatively to the cell.
 Default value is KMKCellVerticalContentLabelsAlignmentCenter.
 */
@property (nonatomic, assign) KMKCellVerticalContentLabelsAlignment verticalContentLabelsAlignment;

/**
 Designated initializer.
 */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

@end
