//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLabels32LayoutMaker.h"

#import "KMKCellConstants.h"
#import "KMKCellLabels32LayoutParams.h"
#import "KMKCellLabels32LayoutInfo.h"
#import "KMKImageViewWithTitle.h"
#import "KMKCell+Utils.h"
#import "KMKHorizPairOfLabelsView.h"


@implementation KMKCellLabels32LayoutMaker

// overriden
// TODO: to calcuate right size
- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size {
    KMKCellLabels32LayoutParams *layoutParams = (KMKCellLabels32LayoutParams *)params;
    KMKCellLabels32LayoutInfo *layoutInfo = [KMKCellLabels32LayoutInfo new];
    
    const CGFloat kCellLeftMargin = KMKCellLeftMargin;
    const CGFloat kImgWithTitleMaxHeight = size.height - KMKCellSeparatorHeight;
    const CGFloat kImgWithTitleMaxWidth = kImgWithTitleMaxHeight; // max width is equal to the max height
    
    const CGFloat kLeftLabelPairsMargin = KMKCellHorizontalSpacing;
    const CGFloat kSpacingBetweenLabelPairs = 3.0;
    
    // image with title
    CGRect imageWithTitleFrame = CGRectZero;
    const CGSize kMaxImgWithTitleSize = CGSizeMake(kImgWithTitleMaxWidth, kImgWithTitleMaxHeight);
    CGSize imgWithTitleSize = [KMKImageViewWithTitle preferredSizeForDataSource:layoutParams.imageViewWithTitleLayoutParams
                                                                           size:kMaxImgWithTitleSize];
    
    imageWithTitleFrame.origin.y = floorf((size.height - imgWithTitleSize.height)/2.0);
    imageWithTitleFrame.origin.x = MIN(imageWithTitleFrame.origin.y, kCellLeftMargin);
    imageWithTitleFrame.size = imgWithTitleSize;
    layoutInfo.imageWithTitleFrame = imageWithTitleFrame;
    
    // pairs of labels
    const CGFloat kLabelPairOriginX = imageWithTitleFrame.origin.x + imageWithTitleFrame.size.width +
        (layoutParams.imageViewWithTitleLayoutParams.image ? kLeftLabelPairsMargin : 0.0);
    const CGFloat kLabelPairRightEdgeX = size.width - [KMKCell rightMarginForLayoutParams:layoutParams];
    const CGFloat kLabelPairWidth = kLabelPairRightEdgeX - kLabelPairOriginX;
    
    CGRect firstFrame = CGRectZero;
    CGRect secondFrame = CGRectZero;
    CGRect thirdFrame = CGRectZero;
    
    switch (layoutParams.verticalContentLabelsAlignment) {
        case KMKCellVerticalContentLabelsAlignmentTop: {
            // first pair
            firstFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.firstLabelPairLayoutParams
                                                                              size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            firstFrame.origin.x = kLabelPairOriginX;
            firstFrame.origin.y = KMKCellTopMargin;
            layoutInfo.firstPairOfLabelsFrame = firstFrame;
            
            // second pair
            secondFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.secondLabelPairLayoutParams
                                                                               size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            secondFrame.origin.x = kLabelPairOriginX;
            secondFrame.origin.y = firstFrame.origin.y + firstFrame.size.height + kSpacingBetweenLabelPairs;
            layoutInfo.secondPairOfLabelsFrame = secondFrame;
            
            // third pair
            thirdFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.thirdLabelPairLayoutParams
                                                                              size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            thirdFrame.origin.x = kLabelPairOriginX;
            thirdFrame.origin.y = secondFrame.origin.y + secondFrame.size.height + kSpacingBetweenLabelPairs;
            layoutInfo.thirdPairOfLabelsFrame = thirdFrame;
            break;
        }
            
        case KMKCellVerticalContentLabelsAlignmentCenter: {
            // first pair
            firstFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.firstLabelPairLayoutParams
                                                                              size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            firstFrame.origin.x = kLabelPairOriginX;
            firstFrame.origin.y = 0.0;
            
            // second pair
            secondFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.secondLabelPairLayoutParams
                                                                               size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            secondFrame.origin.x = kLabelPairOriginX;
            
            thirdFrame.size = [KMKHorizPairOfLabelsView preferredSizeForDataSource:layoutParams.thirdLabelPairLayoutParams
                                                                              size:CGSizeMake(kLabelPairWidth, CGFLOAT_MAX)];
            thirdFrame.origin.x = kLabelPairOriginX;
            
            CGFloat kAllLabelPairsHeight = firstFrame.size.height + secondFrame.size.height + thirdFrame.size.height + 2 * kSpacingBetweenLabelPairs;
            
            firstFrame.origin.y = floorf((size.height - kAllLabelPairsHeight)/2.0);
            secondFrame.origin.y = firstFrame.origin.y + firstFrame.size.height + kSpacingBetweenLabelPairs;
            thirdFrame.origin.y = secondFrame.origin.y + secondFrame.size.height + kSpacingBetweenLabelPairs;
            
            layoutInfo.firstPairOfLabelsFrame = firstFrame;
            layoutInfo.secondPairOfLabelsFrame = secondFrame;
            layoutInfo.thirdPairOfLabelsFrame = thirdFrame;
            break;
        }
            
        default:
            NSAssert(NO, @"KMKCellVerticalContentLabelsAlignmentBottom is not implemented or you set invalid value for verticalContentLabelsAlignment propety!");
            break;
    }
    
    // final calculations
    CGFloat contingentRightHeight = MAX(KMKCellBottomMargin +
                                        (thirdFrame.origin.y + thirdFrame.size.height - firstFrame.origin.y) +
                                        KMKCellBottomMargin,
                                        KMKCellBottomMargin + imageWithTitleFrame.size.height + KMKCellTopMargin);
    CGFloat contingentRightWidth = size.width; // left is it is
    layoutInfo.contingentRightSize = CGSizeMake(contingentRightWidth, contingentRightHeight);
    
    return layoutInfo;
}

@end
