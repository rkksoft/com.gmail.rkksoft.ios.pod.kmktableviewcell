//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLabels32.h"

#import "KMKHorizPairOfLabelsView.h"
#import "KMKCellLabels32+Subclass.h"
#import "KMKCellLabels32LayoutInfo.h"
#import "KMKCellLabels32LayoutParams.h"
#import "KMKCellLabels32LayoutMaker.h"


@interface KMKCellLabels32 ()

@property (strong, nonatomic) KMKHorizPairOfLabelsView *firstLabelPair;
@property (strong, nonatomic) KMKHorizPairOfLabelsView *secondLabelPair;
@property (strong, nonatomic) KMKHorizPairOfLabelsView *thirdLabelPair;

@end


@implementation KMKCellLabels32

#pragma mark
#pragma mark Class methods

+ (CGSize)preferredSizeForDataSource:(id<KMKCellLayoutParams>)params size:(CGSize)size {
    KMKCellLabels32LayoutInfo *layoutInfo = [self layoutInfo_cellLabels32_forParams:params
                                                                           cellSize:size];
    return layoutInfo.contingentRightSize;
}

#pragma mark
#pragma mark Property

// labels

- (UILabel *)textLabel {
    return self.firstLabelPair.firstLabel;
}

- (UILabel *)detailTextLabel {
    return self.firstLabelPair.secondLabel;
}

- (UILabel *)text1Label {
    return self.secondLabelPair.firstLabel;
}

- (UILabel *)detailText1Label {
    return self.secondLabelPair.secondLabel;
}

- (UILabel *)text2Label {
    return self.thirdLabelPair.firstLabel;
}

- (UILabel *)detailText2Label {
    return self.thirdLabelPair.secondLabel;
}

// priorities

- (void)setTextAndDetailTextPairPriority:(KMKLabelPairPriority)priority {
    self.firstLabelPair.labelPairPriority = priority;
}

- (void)setText1AndDetailText1PairPriority:(KMKLabelPairPriority)priority {
    self.firstLabelPair.labelPairPriority = priority;
}

- (void)setText2AndDetailText2PairPriority:(KMKLabelPairPriority)priority {
    self.firstLabelPair.labelPairPriority = priority;
}

// image

- (UIImageView *)imageView {
    return self.imageViewWithTitle.imageView;
}

#pragma mark
#pragma mark Allocation

// TODO: move the whole class to the KMKCell as separate style
// this allow to avoid computational burder for making layout in super class
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    // used style KMKTableViewCellStyleNone to avoid computational burden (particularly when invoking super's layaout)
    self = [super initWithCellStyle:KMKTableViewCellStyleNone reuseIdentifier:reuseIdentifier layoutMaker:nil];
    if (self) {
        self.verticalContentLabelsAlignment = KMKCellVerticalContentLabelsAlignmentCenter;
        
        _imageViewWithTitle = [KMKImageViewWithTitle new];
        [self.contentView addSubview:_imageViewWithTitle];        
        _firstLabelPair = [KMKHorizPairOfLabelsView new];
        [self.contentView addSubview:_firstLabelPair];
        _secondLabelPair = [KMKHorizPairOfLabelsView new];
        [self.contentView addSubview:_secondLabelPair];
        _thirdLabelPair = [KMKHorizPairOfLabelsView new];
        [self.contentView addSubview:_thirdLabelPair];
        
        // defaults
        self.textAndDetailTextPairPriority = KMKLabelPairPrioritySecond;
        self.text1AndDetailText1PairPriority = KMKLabelPairPrioritySecond;
        self.text2AndDetailText2PairPriority = KMKLabelPairPrioritySecond;
    }
    return self;
}

#pragma mark
#pragma mark Public

// overriden
- (id<KMKCellLayoutParams>)layoutParams {
    KMKCellLabels32LayoutParams *layoutParams = [KMKCellLabels32LayoutParams layoutParamsWithLayoutParams:[super layoutParams]];
    layoutParams.imageViewWithTitleLayoutParams = [self.imageViewWithTitle layoutParams];
    layoutParams.firstLabelPairLayoutParams = [self.firstLabelPair layoutParams];
    layoutParams.secondLabelPairLayoutParams = [self.secondLabelPair layoutParams];
    layoutParams.thirdLabelPairLayoutParams = [self.thirdLabelPair layoutParams];
    layoutParams.verticalContentLabelsAlignment = self.verticalContentLabelsAlignment;
    return layoutParams;
}

#pragma mark
#pragma mark Protected

// overriden
- (void)applyLayoutInfo:(id<KMKCellLayoutInfo>)layoutInfo {   
    KMKCellLabels32LayoutInfo *labels32LayoutInfo = (KMKCellLabels32LayoutInfo *)layoutInfo;
    self.imageViewWithTitle.frame = labels32LayoutInfo.imageWithTitleFrame;
    self.firstLabelPair.frame = labels32LayoutInfo.firstPairOfLabelsFrame;
    self.secondLabelPair.frame = labels32LayoutInfo.secondPairOfLabelsFrame;
    self.thirdLabelPair.frame = labels32LayoutInfo.thirdPairOfLabelsFrame;
}

// overriden
+ (id<KMKCellLayoutMaker>)defaultLayoutMakerForCellStyle:(KMKTableViewCellStyle)cellStyle {
    static KMKCellLabels32LayoutMaker *layoutMaker = nil;
    if (layoutMaker == nil) {
        layoutMaker = [KMKCellLabels32LayoutMaker layoutMaker];
    }   
    return layoutMaker;
}

@end
