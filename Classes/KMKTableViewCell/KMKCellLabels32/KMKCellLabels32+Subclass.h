//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLabels32.h"

#import "KMKCell+Subclass.h"


@interface KMKCellLabels32 (Subclass)

+ (id<KMKCellLayoutInfo>)layoutInfo_cellLabels32_forParams:(id<KMKCellLayoutParams>)params
                                                  cellSize:(CGSize)size;

@end
