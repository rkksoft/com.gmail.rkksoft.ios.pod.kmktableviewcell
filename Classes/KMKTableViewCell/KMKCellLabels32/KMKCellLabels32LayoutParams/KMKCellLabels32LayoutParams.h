//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutParamsImpl.h"
#import "KMKHorizPairOfLabelsViewLayoutParams.h"
#import "KMKImageViewWithTitleLayoutParams.h"


@interface KMKCellLabels32LayoutParams : KMKCellLayoutParamsImpl

@property (strong, nonatomic) KMKImageViewWithTitleLayoutParams *imageViewWithTitleLayoutParams;

@property (strong, nonatomic) KMKHorizPairOfLabelsViewLayoutParams *firstLabelPairLayoutParams;
@property (strong, nonatomic) KMKHorizPairOfLabelsViewLayoutParams *secondLabelPairLayoutParams;
@property (strong, nonatomic) KMKHorizPairOfLabelsViewLayoutParams *thirdLabelPairLayoutParams;

/**
 Determines vertical alignment of label pairs relatively to the cell.
 */
@property (nonatomic, assign) KMKCellVerticalContentLabelsAlignment verticalContentLabelsAlignment;

@end
