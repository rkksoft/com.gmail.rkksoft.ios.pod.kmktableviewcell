//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

/**
 Used to specify which label must be displayed wholly (without cutting).
 
 Label with higher priority is ussumed to have one line. Label with lower priority can be multiline. Text in both label is vertically centrally aligned.
 */
typedef enum {
    // First label should be displayed wholly.
    KMKLabelPairPriorityFirst = 1 << 0,
    // Second label should be displayed wholly.
    KMKLabelPairPrioritySecond = 1 << 1
} KMKLabelPairPriority;

