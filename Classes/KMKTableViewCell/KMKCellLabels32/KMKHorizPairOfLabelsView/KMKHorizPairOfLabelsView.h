//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKHorizPairOfLabelsTypes.h"
#import "KMKHorizPairOfLabelsViewLayoutParams.h"

//TODO: it is necessary to descrive layout details
// Very similar functionality is provided by KMKAttributeView - think of that

/**
 Label with higher priority is displayed wholly. The other label may be multiline and can occupy the rest place.
 
 If the second label has the alignment different then left, then width of component (self) when invoking `sizeThatFit` doesn't change.
 This allows display the second label in some significant distance from the first (alignment must be set properly)
 
 Text in both labels is vertically centered.
 */
@interface KMKHorizPairOfLabelsView : UIView

/**
 Return the most appropriate size but not the larger than the specified size.
 May be useful for cells when it is neccessary to find the height of cell and avoid cell's creation. In such situation there
 is no instance of this class and it is needed to make layout of cell's elements.
 */
+ (CGSize)preferredSizeForDataSource:(KMKHorizPairOfLabelsViewLayoutParams *)params size:(CGSize)size;

@property (retain, nonatomic) UILabel *firstLabel;
@property (retain, nonatomic) UILabel *secondLabel;

/**
 Determines which label of pair should be displayed wholly (without cutting). 
 Label with higher priority is ussumed to have one line. Label with lower priority can be multiline. 
 Text in both labels is vertically centered.
 Default value - KMKLabelPairPrioritySecond.
 */
// TODO: arrange for labels in case of KMKLabelPairPriorityFirst | KMKLabelPairPrioritySecond cover available space equally.
// By now this case is equivalent KMKLabelPairPriorityFirst
@property (nonatomic, assign) KMKLabelPairPriority labelPairPriority;

- (KMKHorizPairOfLabelsViewLayoutParams *)layoutParams;

@end
