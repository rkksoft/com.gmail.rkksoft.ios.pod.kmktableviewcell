//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKHorizPairOfLabelsView.h"


#import "KMKHorizPairOfLabelsLayoutInfo.h"
#import "NSString+KMKUtils.h"


static const CGFloat kInternalSpacing = 5.0;


// Description of layout.
typedef struct {
    CGRect firstFrame;
    CGRect secondFrame;
    CGFloat preferredHeight;
} LabelPairLayoutInfo;


@implementation KMKHorizPairOfLabelsView

#pragma mark
#pragma mark Class methods

+ (CGSize)preferredSizeForDataSource:(KMKHorizPairOfLabelsViewLayoutParams *)params size:(CGSize)size {
    KMKHorizPairOfLabelsLayoutInfo *layoutInfo = [self horizPairOfLabelsLayoutInfoForParams:params
                                                                                   viewSize:size];
    return layoutInfo.contingentRightSize;
}

#pragma mark
#pragma mark Allocation

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self init_common];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self init_common];
    }
    return self;
}

- (void)init_common {
    self.clipsToBounds = YES;
    _labelPairPriority = KMKLabelPairPrioritySecond;
    
    _firstLabel = [UILabel new];
    _firstLabel.font = [UIFont systemFontOfSize:14.0];
    _firstLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_firstLabel];
    
    _secondLabel = [UILabel new];
    _secondLabel.font = [UIFont systemFontOfSize:14.0];
    _secondLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_secondLabel];
}

#pragma mark
#pragma mark Protected

/**
 The width is left as it is.
 */
- (CGSize)sizeThatFits:(CGSize)size {
    KMKHorizPairOfLabelsLayoutInfo *layoutInfo = [[self class] horizPairOfLabelsLayoutInfoForParams:[self layoutParams]
                                                                                           viewSize:size];
    return layoutInfo.contingentRightSize;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    // debug
//    self.firstLabel.backgroundColor = [UIColor grayColor];
//    self.secondLabel.backgroundColor = [UIColor blueColor];
    
    KMKHorizPairOfLabelsLayoutInfo *layoutInfo = [[self class] horizPairOfLabelsLayoutInfoForParams:[self layoutParams]
                                                                                           viewSize:self.frame.size];
    self.firstLabel.frame = layoutInfo.firstFrame;
    self.secondLabel.frame = layoutInfo.secondFrame;
}

#pragma mark
#pragma mark Private

+ (KMKHorizPairOfLabelsLayoutInfo *)horizPairOfLabelsLayoutInfoForParams:(KMKHorizPairOfLabelsViewLayoutParams *)params
                                                                viewSize:(CGSize)size {
    
    KMKHorizPairOfLabelsLayoutInfo *layoutInfo = [KMKHorizPairOfLabelsLayoutInfo new];
    
    CGRect firstFrame = CGRectZero;
    CGRect secondFrame = CGRectZero;
    CGSize firstSize = CGSizeZero;
    CGSize secondSize = CGSizeZero;
    
    switch (params.labelPairPriority) {
            
        case KMKLabelPairPriorityFirst: {
            firstSize = [params.firstText kmk_oneLineSizeConstrainedToWidth:size.width
                                                                       font:params.firstTextFont
                                                              lineBreakMode:params.firstTextLineBreakMode];
            
            firstFrame.size = firstSize;
            
            const CGFloat kSpacing = [params.firstText length] ? kInternalSpacing : 0.0; // if firstText is absent then all place for second lbl
            secondFrame.origin.x = firstFrame.origin.x + firstFrame.size.width + kSpacing;
            const CGFloat kSecondMaxWidth = size.width - secondFrame.origin.x;
            secondSize.width = kSecondMaxWidth;
            
            secondSize.height = [params.secondText kmk_multilineSizeConstrainedToWidth:kSecondMaxWidth
                                                                         numberOfLines:params.secondTextNumberOfLines
                                                                                  font:params.secondTextFont
                                                                          lineBreakMode:params.secondTextLineBreakMode].height;
            
            secondFrame.size = secondSize;
            
            break;
        }
        case KMKLabelPairPrioritySecond: {
            secondSize = [params.secondText kmk_oneLineSizeConstrainedToWidth:size.width
                                                                         font:params.secondTextFont
                                                                lineBreakMode:params.secondTextLineBreakMode];
            secondFrame.size = secondSize; // the width may be changed - see more down
            
            const CGFloat kSpacing = [params.secondText length] ? kInternalSpacing : 0.0; // if secondText is absent then all place for first lbl
            const CGFloat kMaxFirstWidth = size.width - secondFrame.size.width - kSpacing;
            firstSize = [params.firstText kmk_multilineSizeConstrainedToWidth:kMaxFirstWidth
                                                                numberOfLines:params.firstTextNumberOfLines
                                                                         font:params.firstTextFont
                                                                lineBreakMode:params.firstTextLineBreakMode];
            
            firstFrame.size = firstSize;
            
            // second label must be beside first label
            secondFrame.origin.x = firstFrame.origin.x + firstFrame.size.width + kInternalSpacing;
            if (params.secondTextAlignment != NSTextAlignmentLeft) {
                secondFrame.size.width = size.width - secondFrame.origin.x;
            }
            
            break;
        }
        default:
            NSAssert(NO, @"Unknown value!");
            break;
    }
    
    layoutInfo.firstFrame = firstFrame;
    layoutInfo.secondFrame = secondFrame;
    
    // layoutInfo.sizeToFit
    // If the second label's textAlignment is not left then the contingent right width is the same as size.width
    CGFloat contingentRightWidth = 0;
    if (params.secondTextAlignment != NSTextAlignmentLeft) {
        contingentRightWidth = secondFrame.origin.x + secondFrame.size.width;
    }
    else {
        contingentRightWidth = size.width;
    }
    
    const CGFloat kHeightToFit = MIN(size.height, MAX(firstSize.height, secondSize.height));
    layoutInfo.contingentRightSize = CGSizeMake(contingentRightWidth, MIN(size.height, kHeightToFit));
    
    return layoutInfo;
}

- (KMKHorizPairOfLabelsViewLayoutParams *)layoutParams {
    KMKHorizPairOfLabelsViewLayoutParams *layoutParams = [KMKHorizPairOfLabelsViewLayoutParams new];
    layoutParams.labelPairPriority = self.labelPairPriority;
    layoutParams.firstText = self.firstLabel.text;
    layoutParams.firstTextFont = self.firstLabel.font;
    layoutParams.firstTextNumberOfLines = self.firstLabel.numberOfLines;
    layoutParams.firstTextLineBreakMode = self.firstLabel.lineBreakMode;
    layoutParams.secondText = self.secondLabel.text;
    layoutParams.secondTextNumberOfLines = self.secondLabel.numberOfLines;
    layoutParams.secondTextFont = self.secondLabel.font;
    layoutParams.secondTextAlignment = self.secondLabel.textAlignment;
    layoutParams.secondTextLineBreakMode = self.secondLabel.lineBreakMode;
    return layoutParams;
}

@end

