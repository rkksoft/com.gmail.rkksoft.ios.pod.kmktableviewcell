//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>


@interface KMKHorizPairOfLabelsLayoutInfo : NSObject

@property (nonatomic, assign) CGRect firstFrame;
@property (nonatomic, assign) CGRect secondFrame;

/**
 The size to fit content of view taking into account the specified layout params and max size.
 */
@property (nonatomic, assign) CGSize contingentRightSize;

@end
