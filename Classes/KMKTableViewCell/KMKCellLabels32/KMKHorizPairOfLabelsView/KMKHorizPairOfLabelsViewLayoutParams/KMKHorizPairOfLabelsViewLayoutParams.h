//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKHorizPairOfLabelsTypes.h"

#import <UIKit/UIKit.h>


@interface KMKHorizPairOfLabelsViewLayoutParams : NSObject

/**
 Determines which label of pair should be displayed wholly (without cutting).
 @see  HorizPairOfLabelsView.labelPairPriority
 */
@property (nonatomic, assign) KMKLabelPairPriority labelPairPriority;

/**
 Text of textLabel.
 */
@property (strong, nonatomic) NSString *firstText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger firstTextNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *firstTextFont;

/**
 lineBreakMode of the first label.
 */
@property (nonatomic, assign) NSLineBreakMode firstTextLineBreakMode;

/**
 Text of textLabel.
 */
@property (strong, nonatomic) NSString *secondText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger secondTextNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *secondTextFont;

/**
 The text alignment of the second label.
 */
@property (nonatomic, assign) NSTextAlignment secondTextAlignment;

/**
 lineBreakMode of the second label.
 */
@property (nonatomic, assign) NSLineBreakMode secondTextLineBreakMode;


@end
