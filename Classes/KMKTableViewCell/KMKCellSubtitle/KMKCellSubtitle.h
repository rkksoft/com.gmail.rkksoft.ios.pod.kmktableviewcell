//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCell.h"

#import "KMKCellTypes.h"
#import "KMKCellLayoutParamsSubtitle.h"


/**
 Resembles UITableViewCell with UITableViewCellStyleSubtitle style. But imageView (if it is set) always has the same left margin (KMKCellLeftMargin).
 Allows to calculate the height needed to normally display content. When calculating the height of the imageView is taken into account.
 
 Метки все вместе центрируется.
 */
@interface KMKCellSubtitle : KMKCell

/**
 Designated initilizer.
 */
- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier layoutMaker:(id<KMKCellLayoutMaker>)layoutMaker;

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)initWithFrame:(CGRect)frame;

/**
 Initialize cell with reuse identifier equal to nil.
 */
- (id)init;

/**
 Additional label located at the bottom of the cell.
 */
@property (nonatomic, readonly) UILabel *thirdLabel;

/**
 Vertical alignment of labels. 
 Image view is not affected by this property.
 Default value is KMKSubtitleCellContentVerticalAlignmentCenter.
 */
@property (nonatomic, assign) KMKCellVerticalContentLabelsAlignment contentVerticalLabelsAlignment;

/**
 Minimal value for top and bottom margin of imageView. 
 It is supposed that imageView is centered vertically if height is big enough (rather than top or bottom-alingned vertically).
 ImageView is reducuded by height to meet this constraint.
 When caclulating preferred heith the value of this property is ignored.  
 Default value CGFLOAT_MIN what means vertical centering and keeping original size of image.
 */
@property (nonatomic, assign) CGFloat minBottomAndTopMarginForImage;

/**
 Retrurns layout parameters of cell.
 Layout parameters are usually used to determine height of the cell.
 */
- (KMKCellLayoutParamsSubtitle *)layoutParams;

@end
