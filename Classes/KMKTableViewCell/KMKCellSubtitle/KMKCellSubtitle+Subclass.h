//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellSubtitle.h"

#import "KMKCell+Subclass.h"
#import "KMKCellLayoutParamsSubtitle.h"


@interface KMKCellSubtitle (Subclass)

// Description of layout.
typedef struct {
    CGRect textFrame;
    CGRect detailTextFrame;
    CGRect thirdTextFrame;
    CGRect imageFrame;
    CGFloat preferredHeight;
} KMKTCellSubtitleLayoutInfo;

+ (KMKTCellSubtitleLayoutInfo)tableViewCellSubtitleLayoutInfoForParams:(KMKCellLayoutParamsSubtitle *)params
                                                                      cellSize:(CGSize)size;

/**
 Right margin for labels.
 Can be helpfull when implementing tableViewCellSubtitleLayoutInfoForParams:cellSize: in subclass.
 */
+ (CGFloat)rightMarginForLayoutParams:(KMKCellLayoutParamsSubtitle *)layoutParams;

/**
 Параметры разметки, снятые с ячейки.
 Используются обычно как аргумент для tableViewCellSubtitleLayoutInfoForParams:cellSize:.
 */
- (KMKCellLayoutParamsSubtitle *)layoutParams;

@end
