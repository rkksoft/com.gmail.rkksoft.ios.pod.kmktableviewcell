//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutMakerSubtitle.h"

#import "NSString+KMKUtils.h"

#import "KMKCellLayoutInfoSubtitle.h"
#import "KMKCellConstants.h"
#import "KMKCell+Utils.h"
#import "KMKCellLayoutParamsSubtitle.h"
#import "KMKCellLayoutMakerImpl+Subclass.h"


@implementation KMKCellLayoutMakerSubtitle

#pragma mark <KMKCellLayoutMaker>

- (id<KMKCellLayoutInfo>)layoutInfoForParams:(id<KMKCellLayoutParams>)params
                                    cellSize:(CGSize)size {
    
    KMKCellLayoutParamsSubtitle *cellLayoutParamsSubtitle = (KMKCellLayoutParamsSubtitle *)params;
    KMKCellLayoutInfoSubtitle *layoutInfo = [KMKCellLayoutInfoSubtitle layoutInfo];
    
    // subclasses can use other value for left margin, this is central point for managing the value of margin
    const CGFloat kCellLeftMargin = self.leftMargin;
    
    const CGFloat kImageTopMargin = KMKCellTopMargin;
    const CGFloat kImageBottomMargin = KMKCellBottomMargin;
    
    // image
    // frame is calculated
    CGRect imageFrame = CGRectZero;
    imageFrame.origin.x = kCellLeftMargin;
    imageFrame.size = [cellLayoutParamsSubtitle image].size;
    imageFrame.origin.y = floorf((size.height - imageFrame.size.height)/2.0);
    layoutInfo.imageFrame = imageFrame;
    
    // constants for labels
    const CGFloat kOriginXOfLabels = imageFrame.origin.x + imageFrame.size.width + (cellLayoutParamsSubtitle.image ? KMKCellHorizontalSpacing : 0.0);
    const CGFloat kWidthOfLabels = size.width - kOriginXOfLabels - [KMKCell rightMarginForLayoutParams:cellLayoutParamsSubtitle];
    
    // textLabel
    CGRect textFrame = CGRectZero;
    textFrame.origin.x = kOriginXOfLabels;
    textFrame.origin.y = KMKCellTopMargin;
    textFrame.size.width = kWidthOfLabels;
    textFrame.size.height = [cellLayoutParamsSubtitle.text kmk_multilineSizeConstrainedToWidth:kWidthOfLabels
                                                                                 numberOfLines:cellLayoutParamsSubtitle.textNumberOfLines
                                                                                          font:cellLayoutParamsSubtitle.textFont
                                                                                 lineBreakMode:cellLayoutParamsSubtitle.textLineBreakMode].height;
    // detailTextLabel
    CGRect detailTextFrame = CGRectZero;
    detailTextFrame.origin.x = kOriginXOfLabels;
    detailTextFrame.origin.y = textFrame.origin.y + textFrame.size.height;
    detailTextFrame.size.width = kWidthOfLabels;
    detailTextFrame.size.height = [cellLayoutParamsSubtitle.detailText kmk_multilineSizeConstrainedToWidth:kWidthOfLabels
                                                                                             numberOfLines:cellLayoutParamsSubtitle.detailTextNumberOfLines
                                                                                                      font:cellLayoutParamsSubtitle.detailTextFont
                                                                                             lineBreakMode:cellLayoutParamsSubtitle.detailTextLineBreakMode].height;
    // thirdTextLabel
    CGRect thirdTextFrame = CGRectZero;
    thirdTextFrame.origin.x = kOriginXOfLabels;
    thirdTextFrame.origin.y = detailTextFrame.origin.y + detailTextFrame.size.height;
    thirdTextFrame.size.width = kWidthOfLabels;
    thirdTextFrame.size.height = [cellLayoutParamsSubtitle.thirdText kmk_multilineSizeConstrainedToWidth:kWidthOfLabels
                                                                                           numberOfLines:cellLayoutParamsSubtitle.thirdTextNumberOfLines
                                                                                                    font:cellLayoutParamsSubtitle.thirdTextFont
                                                                                           lineBreakMode:cellLayoutParamsSubtitle.thirdTextLineBreakMode].height;
    
    // calculating contingent right size (preferred height)
    CGFloat congingentRightHeght = 0.0;
    const CGFloat kPreferredHeightForLabels = KMKCellTopMargin + KMKCellBottomMargin +
                                            (thirdTextFrame.origin.y + thirdTextFrame.size.height - textFrame.origin.y);
    
    if (cellLayoutParamsSubtitle.image == nil && [cellLayoutParamsSubtitle.text length] == 0 && [cellLayoutParamsSubtitle.detailText length] == 0 && [cellLayoutParamsSubtitle.thirdText length] == 0) {
        congingentRightHeght = 0.0;
    }
    else {
        congingentRightHeght = MAX(kImageTopMargin + imageFrame.size.height + kImageBottomMargin,
                                         kPreferredHeightForLabels);
    }
    layoutInfo.contingentRightSize = CGSizeMake(size.width, congingentRightHeght);
    
    
    // centering and cutting labels if needed
    CGFloat dY = 0.0;
    // TODO: обработана только ситуация, когда detailTextLabel не вмещается. Еще надо обработать textLabel и thirdTextLabel.
    if (kPreferredHeightForLabels > size.height) { // if height is not large enough to fit labels
        // If detailTextLabel doesn't fit into cell taking into account top and bottom margins
        if (detailTextFrame.origin.y + detailTextFrame.size.height + KMKCellBottomMargin > size.height) {
            
            NSInteger detailNumberOfLines = floorf((size.height - detailTextFrame.origin.y - KMKCellBottomMargin)/[[cellLayoutParamsSubtitle detailTextFont] lineHeight]);
            if (detailNumberOfLines >= 0) {
                detailTextFrame.size.height = [cellLayoutParamsSubtitle.thirdText kmk_multilineSizeConstrainedToWidth:kWidthOfLabels
                                                                                                        numberOfLines:detailNumberOfLines
                                                                                                                 font:cellLayoutParamsSubtitle.detailTextFont
                                                                                                        lineBreakMode:cellLayoutParamsSubtitle.detailTextLineBreakMode].height;
                
                dY = floorf((size.height - detailTextFrame.origin.y - detailTextFrame.size.height - KMKCellBottomMargin)/2.0);
            }
            else {
                //TODO: ситуация, когда textLabel не вмещается, не обработана
                dY = 0;
            }
        }
    }
    else {
        switch ([cellLayoutParamsSubtitle contentVerticalLabelsAlignment]) {
            case KMKCellVerticalContentLabelsAlignmentTop:
                dY = 0.0;
                break;
            case KMKCellVerticalContentLabelsAlignmentCenter: {
                dY = floorf((size.height - thirdTextFrame.origin.y - thirdTextFrame.size.height - KMKCellBottomMargin)/2.0);
                break;
            }
            case KMKCellVerticalContentLabelsAlignmentBottom: {
                NSAssert(NO, @"Not implemented!");
                break;
            }
            default:
                NSAssert(NO, @"The App error!");
                break;
        }
    }
    
    textFrame = CGRectOffset(textFrame, 0.0, dY);
    detailTextFrame = CGRectOffset(detailTextFrame, 0.0, dY);
    thirdTextFrame = CGRectOffset(thirdTextFrame, 0.0, dY);
    
    layoutInfo.textFrame = textFrame;
    layoutInfo.detailTextFrame = detailTextFrame;
    layoutInfo.thirdTextFrame = thirdTextFrame;
    
    return layoutInfo;
}

@end
