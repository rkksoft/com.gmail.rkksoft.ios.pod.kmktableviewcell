//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellLayoutInfoImpl.h"


@interface KMKCellLayoutInfoSubtitle : KMKCellLayoutInfoImpl

@property (nonatomic, assign) CGRect thirdTextFrame;

@end
