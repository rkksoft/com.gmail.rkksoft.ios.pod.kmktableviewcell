//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import "KMKCellSubtitle.h"

#import "KMKCell+Subclass.h"
#import "KMKCellLayoutParamsSubtitle.h"
#import "KMKCellSubtitle+Subclass.h"
#import "KMKCellLayoutMakerSubtitle.h"
#import "KMKCellLayoutInfoSubtitle.h"


@implementation KMKCellSubtitle

#pragma mark
#pragma mark Allocation

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier layoutMaker:(id<KMKCellLayoutMaker>)layoutMaker {
    if (self = [super initWithCellStyle:KMKTableViewCellStyleSubtitle
                        reuseIdentifier:reuseIdentifier
                            layoutMaker:layoutMaker]) {
        [self init_common_subtitle];
    }
    return self;
}

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    return [self initWithReuseIdentifier:reuseIdentifier layoutMaker:nil];
}

- (id)initWithFrame:(CGRect)frame {
    self = [self initWithReuseIdentifier:nil];
    if (self) {
        self.frame = frame;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self init_common_subtitle];
        [self addSubview:self.textLabel];
        [self addSubview:self.detailTextLabel];
    }
    return self;
}

- (id)init {
    return [self initWithReuseIdentifier:nil];
}


#pragma mark
#pragma mark Protected

/**
 Perfom applying layout description from KMKCellLayoutInfo to the cell.
 Method is invoked by layoutSubviews method.
 Subclasses may use their own implementation of KMKCellLayoutInfo and may use additional attributes.
 It is not obligatory to invoke super in subclasses.
 */
- (void)applyLayoutInfo:(id<KMKCellLayoutInfo>)layoutInfo {
    // debug
//    self.textLabel.backgroundColor = [UIColor redColor];
//    self.detailTextLabel.backgroundColor = [UIColor orangeColor];
//    self.thirdLabel.backgroundColor = [UIColor yellowColor];
    
    KMKCellLayoutInfoSubtitle *cellLayoutInfoSubtitle = (KMKCellLayoutInfoSubtitle *)layoutInfo;
    self.imageView.frame = cellLayoutInfoSubtitle.imageFrame;
    self.textLabel.frame = cellLayoutInfoSubtitle.textFrame;
    self.detailTextLabel.frame = cellLayoutInfoSubtitle.detailTextFrame;
    self.thirdLabel.frame = cellLayoutInfoSubtitle.thirdTextFrame;
}

// overriden
+ (id<KMKCellLayoutMaker>)defaultLayoutMakerForCellStyle:(KMKTableViewCellStyle)cellStyle {
    static KMKCellLayoutMakerSubtitle *layoutMaker = nil;
    if (layoutMaker == nil) {
        layoutMaker = [KMKCellLayoutMakerSubtitle layoutMaker];
    }
    return layoutMaker;
}

#pragma mark
#pragma mark Private

- (void)init_common_subtitle {
    self.minBottomAndTopMarginForImage = CGFLOAT_MIN;
    self.clipsToBounds = YES;
    self.contentVerticalLabelsAlignment = KMKCellVerticalContentLabelsAlignmentCenter; // default
    
    // font is not set in super that may cause sizeToFit return bad result
    self.textLabel.font = [UIFont boldSystemFontOfSize:18.0]; // defalt value for standard cell
    self.detailTextLabel.font = [UIFont systemFontOfSize:14.0]; // defalt value for standard cell
    
    _thirdLabel = [[UILabel alloc] init];
    _thirdLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:_thirdLabel];
}

- (KMKCellLayoutParamsSubtitle *)layoutParams {
    KMKCellLayoutParamsSubtitle *layoutParams = [KMKCellLayoutParamsSubtitle new];
    layoutParams.text = self.textLabel.text;
    layoutParams.textNumberOfLines = self.textLabel.numberOfLines;
    layoutParams.textFont = self.textLabel.font;
    layoutParams.textLineBreakMode = self.textLabel.lineBreakMode;
    layoutParams.detailText = self.detailTextLabel.text;
    layoutParams.detailTextFont = self.detailTextLabel.font;
    layoutParams.detailTextNumberOfLines = self.detailTextLabel.numberOfLines;
    layoutParams.detailTextLineBreakMode = self.detailTextLabel.lineBreakMode;
    layoutParams.thirdText = self.thirdLabel.text;
    layoutParams.thirdTextNumberOfLines = self.thirdLabel.numberOfLines;
    layoutParams.thirdTextFont = self.thirdLabel.font;    
    layoutParams.thirdTextLineBreakMode = self.thirdLabel.lineBreakMode;
    layoutParams.image = self.imageView.image;
    layoutParams.accessoryType = self.accessoryType;
    layoutParams.contentVerticalLabelsAlignment = self.contentVerticalLabelsAlignment;
    layoutParams.accessoryView = self.accessoryView;
    return layoutParams;
}

@end
