//  Copyright (c) 2015, Yauheni Klishevich.
//  Released under the MIT license.

#import <UIKit/UIKit.h>

#import "KMKCellLayoutParams.h"
#import "KMKCellLayoutParamsImpl.h"
#import "KMKCellTypes.h"


@interface KMKCellLayoutParamsSubtitle : NSObject <KMKCellLayoutParams>

#pragma mark KMKTableViewSubtitleCellLayoutParams

/**
 Cell style.
 */
@property (nonatomic, assign) KMKTableViewCellStyle cellStyle;

/**
 Text of textLabel.
 */
@property (strong, nonatomic) NSString *text;
@property (strong, nonatomic) NSAttributedString *attributedText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger textNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *textFont;

/**
 lineBreakMode of textLabel.
 */
@property (nonatomic, assign) NSLineBreakMode textLineBreakMode;

/**
 Text of detailTextLabel.
 */
@property (strong, nonatomic) NSString *detailText;
@property (strong, nonatomic) NSAttributedString *detailAttributedText;

/**
 Max number of lines in detailTextLabel.
 */
@property (nonatomic, assign) NSInteger detailTextNumberOfLines;

/**
 Font of detailTextLabel.
 */
@property (strong, nonatomic) UIFont *detailTextFont;

/**
 lineBreakMode of detailTextLabel.
 */
@property (nonatomic, assign) NSLineBreakMode detailTextLineBreakMode;

/**
 Text of textLabel.
 */
@property (strong, nonatomic) NSString *thirdText;

/**
 Max number of lines in textLabel.
 */
@property (nonatomic, assign) NSInteger thirdTextNumberOfLines;

/**
 Font of textLabel.
 */
@property (strong, nonatomic) UIFont *thirdTextFont;

/**
 lineBreakMode of thirdTextLabel.
 */
@property (nonatomic, assign) NSLineBreakMode thirdTextLineBreakMode;

/**
 May be nil.
 */
@property (strong, nonatomic) UIImage *image;

/**
 Accessory type of cell.
 */
@property (nonatomic, assign) UITableViewCellAccessoryType accessoryType;

/**
 Vertical alignment of labels.
 */
@property (nonatomic, assign) KMKCellVerticalContentLabelsAlignment contentVerticalLabelsAlignment;

@property (strong, nonatomic) UIView *accessoryView;

@end
