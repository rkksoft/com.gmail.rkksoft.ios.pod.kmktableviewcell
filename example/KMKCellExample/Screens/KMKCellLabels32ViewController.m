//
//  KMKCellLabels32ViewController.m
//  KMKCellExample
//
//  Created by Yauheni Klishevich on 07/11/15.
//  Copyright © 2015 Eugene Klishevich. All rights reserved.
//

#import "KMKCellLabels32ViewController.h"

#import "KMKCellLabels32.h"


@interface KMKCellLabels32ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end


@implementation KMKCellLabels32ViewController

#pragma mark -
#pragma mark UIViewController override

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // When user swipe backthe amount of fading coincides with how far user has panned.
    [_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}


#pragma mark -
#pragma mark Public


#pragma mark <UITableViewDataSource>

- (NSInteger)numberOfSectionsInTableView:(UITableView*)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath
{
    static NSString* const kCellIdentifier = @"CellIdentifier";
    KMKCellLabels32* cell = (KMKCellLabels32 *)[tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[KMKCellLabels32 alloc] initWithReuseIdentifier:kCellIdentifier];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:16.0];
        cell.text1Label.font = [UIFont systemFontOfSize:14.0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"accessory_view"]];
    cell.textLabel.text = @"textLabel";
    cell.text1Label.text = @"text1Label";
    
    return cell;
}

#pragma mark <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64.0;
}

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath
{
    
}

#pragma mark -
#pragma mark Private


@end
