//
//  DetailViewController.h
//  KMKCellExample
//
//  Created by Yauheni Klishevich on 07/11/15.
//  Copyright © 2015 Eugene Klishevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) NSString *detailItem;

@end
